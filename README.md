# Infosec

Infosec is a single-tenant Java web application that helps you:

1.  Know what devices your organization has, and to whom those devices are assigned.
2.  Communicate policies to members of your organization, and allow them to accept or decline those policies.

## License

Infosec is released under version 3 of the [GNU General Public License](https://www.gnu.org/licenses/gpl.txt).

## Notes

* Infosec does not perform any response compression.  I recommend configuring the container or web server to compress responses of type `text/html`, `text/css`, and `application/javascript`.
* Infosec does not apply HTTP response headers to control caching.  I recommend configuring the container or web server to add the `Expires` header to `text/css` and `application/javascript` responses.

