TODO
****

* add device assignment CRUD
* add search by serial number
* custom validation error messages
* show version number & use in assets page (servlet listener to read property & put in context?)
* applications
* Normalize email addresses to their own table, add failure column, use Amazon SES, add periodic calls to get failure information
* EntityManager.merge() v. persist()
* Generic dao.save
* OWASP top ten - 2013: https://www.owasp.org/index.php/Top_10_2013-T10
* Gary McGraw's security guidelines
* client certificates
* stereotypes for devices?

UNDERWAY
****

DONE
****
* bootstrap 3, jquery 2
* "nice" error pages w/ visible response status code
* seed data check upon startup
* Fixed tx issue (no tx if aspectj plugin removed from pom) by removing tx:annotation-driven mode attribute value "aspectj" (defaults to proxy); also removed unnecessary context:spring-configured
* Add AclService impl to enforce rule: cannot edit policy after any responses have been received
* AbstractMethodError on setCharacterStream when saving to @Lob/longtext column  -- see http://stackoverflow.com/questions/14141452/abstractmethoderror-setbinarystream-on-tomcat -- upgraded dbcp from 1.3 to 1.4, solved
* CRUD for policy
* CSRF protection
* upgrade assets to Bootstrap 3 rc1
* persons, save credentials in database
* device assignment (attributed join between device and person)
* messages.properites

POSTPONED
***
* Deploy to Amazon Beanstalk: requires credit card info

OWASP TOP 10
***
* A1 - Injection: mitigated by consistent use of Hibernate
* A2 - Broken Authentication and Session Management: test with Zap proxy?
* A3 - Cross-site scripting: mitigated by consistent use of c:out and other escaping mechanisms 
* A4 - Insecure Direct Object References: mitigated by ACLs, but needs frequent reassessment (i.e. changes could introduce vuln)
* A5 - Security Misconfiguration: partially mitigated by auth by default; test with Zap proxy
* A6 - Sensitive Data Exposure: mitigated -- credentails are stored hashed, and are not exposed
* A7 - Missing Function Level Access Control: mitigated by ACL implementation, but needs frequent reassessment
* A8 - Cross-Site Request Forgery: mitigated
* A9 - Using Components with Known Vulnerabilities: unknown
* A10 - Unvalidated Redirects and Forwards: N/A (no forwards or redirects at this time?)
