/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import java.net.URI;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Sanity checks for access-control logic.
 */
public class CrowdRestAuthenticationProviderTest {
	private final static String BASE_URI = "http://localhost:8095/crowd/rest/usermanagement/latest/authentication";
	CrowdRestAuthenticationProvider rap;

	@Before
	public void before() {
		rap = new CrowdRestAuthenticationProvider(BASE_URI, "infosec", "mypwd1024");
	}
	
	@Test
	public void testBuildAuthenticationUri() {
		URI uri = rap.buildAuthenticationUri("foobar");
		Assert.assertEquals(BASE_URI + "?username=foobar", uri.toString());
	}

	@Test
	public void testBuildPasswordDocument() {
		String built = rap.buildPasswordDocument("supersecret</value>");
		Assert.assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?><password><value>supersecret&lt;/value&gt;</value></password>", built);
	}

	/*
	 * tests against live crowd instance; expects given user to be present.
	@Test
	public void testPassesAuthentication() {
		Assert.assertTrue(rap.passesAuthentication("foobar", "mypwd1024"));
	}
	*/
}
