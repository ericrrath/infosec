/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import java.util.Calendar;
import java.util.Date;

import name.rath.eric.infosec.domain.Device;
import name.rath.eric.infosec.domain.DeviceAssignment;
import name.rath.eric.infosec.domain.DeviceAudit;
import name.rath.eric.infosec.domain.Person;
import name.rath.eric.infosec.domain.Policy;
import name.rath.eric.infosec.domain.PolicyRequest;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.model.Permission;

/**
 * Sanity checks for access-control logic.
 */
public class AclServiceImplTest {
	@Test
	public void testPersonPermissions() {
		Person person = new Person();
		Permission perm = AclServiceImpl.getPermission(person);
		Assert.assertTrue((perm.getMask() & BasePermission.READ.getMask()) == BasePermission.READ.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.WRITE.getMask()) == BasePermission.WRITE.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.DELETE.getMask()) == BasePermission.DELETE.getMask());
		
		person.getAssignments().add(new DeviceAssignment(new Device(), person));
		perm = AclServiceImpl.getPermission(person);
		Assert.assertTrue((perm.getMask() & BasePermission.READ.getMask()) == BasePermission.READ.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.WRITE.getMask()) == BasePermission.WRITE.getMask());
		Assert.assertFalse((perm.getMask() & BasePermission.DELETE.getMask()) == BasePermission.DELETE.getMask());
	}

	@Test
	public void testDevicePermissions() {
		Device device = new Device();
		Permission perm = AclServiceImpl.getPermission(device);
		Assert.assertTrue((perm.getMask() & BasePermission.READ.getMask()) == BasePermission.READ.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.WRITE.getMask()) == BasePermission.WRITE.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.DELETE.getMask()) == BasePermission.DELETE.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.CREATE.getMask()) == BasePermission.CREATE.getMask());

		//released devices shouldn't have CREATE, and devices w/ audits shouldn't have DELETE
		device.setRelease(new Date());
		device.getAudits().add(new DeviceAudit());
		perm = AclServiceImpl.getPermission(device);
		Assert.assertTrue((perm.getMask() & BasePermission.READ.getMask()) == BasePermission.READ.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.WRITE.getMask()) == BasePermission.WRITE.getMask());
		Assert.assertFalse((perm.getMask() & BasePermission.DELETE.getMask()) == BasePermission.DELETE.getMask());
		Assert.assertFalse((perm.getMask() & BasePermission.CREATE.getMask()) == BasePermission.CREATE.getMask());
	}
	
	@Test
	public void testDeviceAuditPermissions() {
		Calendar c = Calendar.getInstance();
		DeviceAudit audit = new DeviceAudit();
		audit.setAuditDate(c.getTime());
		Permission perm = AclServiceImpl.getPermission(audit);
		Assert.assertTrue((perm.getMask() & BasePermission.READ.getMask()) == BasePermission.READ.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.WRITE.getMask()) == BasePermission.WRITE.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.DELETE.getMask()) == BasePermission.DELETE.getMask());
		
		//audits less than a week old should still have WRITE and DELETE
		c.add(Calendar.DATE, -6);
		audit.setAuditDate(c.getTime());
		perm = AclServiceImpl.getPermission(audit);
		Assert.assertTrue((perm.getMask() & BasePermission.READ.getMask()) == BasePermission.READ.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.WRITE.getMask()) == BasePermission.WRITE.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.DELETE.getMask()) == BasePermission.DELETE.getMask());

		//audits less than a week old should not have WRITE or DELETE
		c.add(Calendar.DATE, -2);
		audit.setAuditDate(c.getTime());
		perm = AclServiceImpl.getPermission(audit);
		Assert.assertTrue((perm.getMask() & BasePermission.READ.getMask()) == BasePermission.READ.getMask());
		Assert.assertFalse((perm.getMask() & BasePermission.WRITE.getMask()) == BasePermission.WRITE.getMask());
		Assert.assertFalse((perm.getMask() & BasePermission.DELETE.getMask()) == BasePermission.DELETE.getMask());
	}
	
	@Test
	public void testPolicyPermissions() {
		Policy policy = new Policy();
		Permission perm = AclServiceImpl.getPermission(policy);
		Assert.assertTrue((perm.getMask() & BasePermission.READ.getMask()) == BasePermission.READ.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.WRITE.getMask()) == BasePermission.WRITE.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.DELETE.getMask()) == BasePermission.DELETE.getMask());

		//released devices shouldn't have CREATE, and devices w/ audits shouldn't have DELETE
		PolicyRequest req = new PolicyRequest(policy, "person@example.com", "ABCD1234");
		policy.getRequests().add(req);
		perm = AclServiceImpl.getPermission(policy);
		Assert.assertTrue((perm.getMask() & BasePermission.READ.getMask()) == BasePermission.READ.getMask());
		Assert.assertTrue((perm.getMask() & BasePermission.WRITE.getMask()) == BasePermission.WRITE.getMask());
		Assert.assertFalse((perm.getMask() & BasePermission.DELETE.getMask()) == BasePermission.DELETE.getMask());
	}
}
