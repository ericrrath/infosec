/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import name.rath.eric.infosec.domain.Policy;
import name.rath.eric.infosec.domain.PolicyRequest;

public class InfoSecServiceTest {
	private static final String EMAIL_ADDRESS = "person@example.com";
	private static final String CODE = "abcd1234";
	private InfoSecService service;

	@Before
	public void setUp() throws Exception {
		service = new InfoSecService();
		service.repository = Mockito.mock(Repository.class);
		service.mailSender = new JavaMailSenderImpl();
		service.setBaseUrl("http://localhost:8080/infosec");
		service.setFromAddress("infosec@example.com");
	}

	@Test
	public void testSendPolicyRequests() {
		Policy p = new Policy();
		Set<String> addresses = new HashSet<String>();
		addresses.add(EMAIL_ADDRESS);
		service.sendPolicyRequests(p, addresses);
		Assert.assertEquals(1,  p.getRequests().size());
		Assert.assertEquals(EMAIL_ADDRESS, p.getRequests().iterator().next().getEmailAddress());
	}
	
	/**
	 * Don't create a request if policy has pre-existing request for same address.
	 */
	@Test
	public void testSendPolicyRequestsPreexisting() {
		Policy p = new Policy();
		PolicyRequest r = new PolicyRequest(p, EMAIL_ADDRESS, CODE);
		p.getRequests().add(r);
		Assert.assertEquals(1,  p.getRequests().size());
		Assert.assertEquals(EMAIL_ADDRESS, p.getRequests().iterator().next().getEmailAddress());
		
		Set<String> addresses = new HashSet<String>();
		addresses.add(EMAIL_ADDRESS);
		service.sendPolicyRequests(p, addresses);
		Assert.assertEquals(1,  p.getRequests().size());
		Assert.assertEquals(EMAIL_ADDRESS, p.getRequests().iterator().next().getEmailAddress());
	}

	@Test
	public void testNormalizeEmailAddress() {
		String expected = EMAIL_ADDRESS;
		Assert.assertEquals(expected, InfoSecService.normalizeEmailAddress(EMAIL_ADDRESS));
		Assert.assertEquals(expected, InfoSecService.normalizeEmailAddress("  person@example.com  "));
		Assert.assertEquals(expected, InfoSecService.normalizeEmailAddress("PERSON@example.com"));
		Assert.assertEquals(expected, InfoSecService.normalizeEmailAddress("\"Paul Erson\" <person@example.com>"));
	}
}
