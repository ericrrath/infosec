/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang3.RandomStringUtils;
import org.pegdown.PegDownProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import name.rath.eric.infosec.domain.Device;
import name.rath.eric.infosec.domain.DeviceAssignment;
import name.rath.eric.infosec.domain.DeviceAudit;
import name.rath.eric.infosec.domain.Person;
import name.rath.eric.infosec.domain.PersonCredential;
import name.rath.eric.infosec.domain.Policy;
import name.rath.eric.infosec.domain.PolicyRequest;
import name.rath.eric.infosec.domain.PolicyResponse;

/**
 * InfoSecService provides all business logic.
 */
@Service
@Transactional
public class InfoSecService {
	private static final Logger LOGGER = LoggerFactory.getLogger(InfoSecService.class);
	@Autowired
	protected Repository repository;
	@Autowired
	protected PasswordEncoder passwordEncoder;
	@Autowired
	protected JavaMailSender mailSender;
	@Autowired
	protected SearchDao searchDao;

	private URI baseUri;
	private InternetAddress fromAddress;

	@Value("${application.url}")
	public void setBaseUrl(String url) {
		try {
			URL baseUrl = new URL(url);
			baseUri = baseUrl.toURI();
		} catch (MalformedURLException|URISyntaxException e) {
			LOGGER.error("bad url: " + url, e);
			throw new IllegalArgumentException("bad url: " + url);
		}
	}

	@Value("${email.from}")
	public void setFromAddress(String addr) {
		try {
			fromAddress = new InternetAddress(addr);
		} catch (AddressException ae) {
			LOGGER.error("invalid email from address: " + addr, ae);
			fromAddress = null;
		}
	}

	/* start policy methods */
	public List<Policy> getPolicies() {
		return repository.getPolicies();
	}
	
	public Policy getPolicy(Long id) {
		return repository.get(Policy.class, id);
	}
	
	@Transactional(readOnly=false)
	public Policy addPolicy(Policy policy) {
		policy.setModified(new Date());
		return (Policy)repository.save(policy);
	}

	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#policy, 'WRITE')")
	public Policy savePolicy(Policy policy) {
		policy.setModified(new Date());
		return (Policy)repository.save(policy);
	}
	
	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#policy, 'DELETE')")
	public void deletePolicy(Policy policy) {
		//TODO: should delete be permitted if policy has pending requests?  completed requests?
		repository.delete(policy);
	}
	
	@Transactional(propagation=Propagation.SUPPORTS)
	@Cacheable(value="renderedPolicies", key="#policy.getCacheKey()")
	public String getPolicyRendered(Policy policy) {
		return new PegDownProcessor().markdownToHtml(policy.getBody());
	}
	
	public PolicyRequest getPolicyRequest(String code) {
		return repository.getPolicyRequest(code);
	}
	
	/**
	 * Iterates over addresses, creates requests for each.  Addresses with pre-existing requests should be ignored.
	 * @param policy target of requests
	 * @param emailAddresses set of email address strings
	 */
	@Transactional(readOnly = false)
	public void sendPolicyRequests(Policy policy, Set<String> emailAddresses) {
		for (String addr : emailAddresses) {
			String normalized = normalizeEmailAddress(addr);
			if (normalized != null) {
				PolicyRequest req = new PolicyRequest(policy, normalized, RandomStringUtils.randomAlphanumeric(50));
				policy.getRequests().add(req);
				repository.add(req);
				SimpleMailMessage msg = new SimpleMailMessage();
				msg.setTo(addr);
				if (fromAddress != null) {
					msg.setFrom(fromAddress.toString());
				}
				msg.setSubject("Infosec: policy request");
				UriComponentsBuilder builder = UriComponentsBuilder.fromUri(baseUri);
				builder.path("public/policies/request");
				builder.queryParam("code", req.getCode());
				String url = builder.build().toUriString();
				msg.setText(String.format("Please visit %s, review the policy, and accept or decline.", url));
				try {
					LOGGER.error("sending to <{}> message: {}", addr, msg.getText());
					this.mailSender.send(msg);
				} catch (MailException me) {
					LOGGER.error("couldn't send email message to " + addr, me);
				}
			}
		}
	}
	
	/**
	 * Records response for given request.
	 * @param request target of response
	 * @param ipAddress client address used to respond to policy request
	 * @param accept true if recipient accepted policy request, false otherwise
	 * @throws IllegalArgumentException if given request already has response
	 */
	@Transactional(readOnly=false)
	public void savePolicyResponse(PolicyRequest request, String ipAddress, Boolean accept) {
		Assert.isNull(request.getPolicyResponse(), request + " already has response");
		
		PolicyResponse response = new PolicyResponse();
		response.setIpAddress(ipAddress);
		response.setId(request.getId());
		response.setAccept(accept);
		response.setResponseDate(new Date());
		request.setPolicyResponse(response);

		repository.add(response);
		//TODO: send email w/ policy text?
	}
	/* end policy methods */

	/* start device methods */
	public List<Device> getDevices(Device.Type type) {
		return repository.getDevices(type);
	}

	public List<String> getDeviceNames() {
		return repository.getDeviceNames();
	}

	@PostAuthorize("hasPermission(returnObject, 'READ')")
	public Device getDevice(Long id) {
		return repository.get(Device.class, id);
	}

	@Transactional(readOnly=false)
	//TODO: what security should be asserted here?
	public void addDevice(Device device) {
		repository.add(device);
		searchDao.add(device);
	}

	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#device, 'WRITE')")
	public Device saveDevice(Device device) {
		return (Device)repository.save(device);
	}

	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#device, 'DELETE')")
	public void deleteDevice(Device device) {
		repository.delete(device);
	}

	/**
	 * Create a new association between the given device and person.
	 * @param device to be assigned to person
	 * @param person accepting the device
	 * @return new assignment with assignment date set to now, release date left null
	 * @throws IllegalArgumentException is device has existing unreleased assignment
	 */
	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#device, 'WRITE') and hasPermission(#device, 'CREATE')")
	public DeviceAssignment assign(Device device, Person person) {
		if (device.getAssignments().stream().anyMatch(da -> da.getReleased() == null)) {
			throw new IllegalArgumentException("cannot assign a device with existing unreleased assignment");
		}
		DeviceAssignment assignment = new DeviceAssignment(device, person);
		repository.save(assignment);
		return assignment;
	}

	/**
	 * Sets release date of given assignment.
	 * @param assignment to be released
	 * @return modified assignment
	 * @throws IllegalArgumentException if assignment already released
	 */
	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#assignment.device, 'WRITE') and hasPermission(#assignment.device, 'CREATE')")
	public DeviceAssignment release(DeviceAssignment assignment) {
		if (assignment.getReleased() != null) {
			throw new IllegalArgumentException("cannot assign a device with existing unreleased assignment");
		}
		assignment.setReleased(new Date());
		repository.save(assignment);
		return assignment;
	}

	public DeviceAssignment getDeviceAssignment(Long id) {
		return repository.get(DeviceAssignment.class, id);
	}

	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#device, 'WRITE') and hasPermission(#device, 'CREATE')")
	public DeviceAudit addDeviceAudit(Device device, DeviceAudit audit) {
		audit.setDevice(device);
		repository.add(audit);
		searchDao.add(audit);
		return audit;
	}

	@PostAuthorize("hasPermission(returnObject, 'READ')")
	public DeviceAudit getDeviceAudit(Long id) {
		return repository.get(DeviceAudit.class, id);
	}

	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#audit, 'WRITE')")
	public DeviceAudit saveDeviceAudit(DeviceAudit audit) {
		repository.save(audit);
		searchDao.add(audit);
		return audit;
	}

	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#audit, 'DELETE')")
	public void deleteDeviceAudit(DeviceAudit audit) {
		repository.delete(audit);
	}

	public Map<Long, Date> getMostRecentAuditDates() {
		return repository.getMostRecentAuditDates();
	}

	/**
	 * @return map of device id to person to which the device
	 * is current assigned.  devices not currently assigned 
	 * are excluded (i.e. there should be no null keys or values)
	 */
	public Map<Long, Person> getCurrentAssignments() {
		return repository.getCurrentDeviceAssignments().stream().collect(Collectors.toMap(da -> da.getDevice().getId(), da -> da.getPerson()));
	}

	/**
	 * @param start restricts device results by date
	 * @param end restricts device results by date
	 * @param owned limit results to owned/not-owned if non-null
	 * @return Map of acquired|released|retained to list of devices in the given state
	 */
	public Map<String, List<Device>> getDeviceSummary(Date start, Date end, Boolean owned) {
		Map<String, List<Device>> summary = new HashMap<>();
		summary.put("acquired", new ArrayList<Device>());
		summary.put("released", new ArrayList<Device>());
		summary.put("retained", new ArrayList<Device>());
		List<Device> all = repository.getDevices(null);
		for (Device d: all) {
			if (owned == null || owned.equals(d.getOwned())) {
				if (d.getAcquisition().after(start) && d.getAcquisition().before(end)) {
					summary.get("acquired").add(d);
				}
				if (d.getRelease() != null && d.getRelease().after(start) && d.getRelease().before(end)) {
					summary.get("released").add(d);
				}
				if (d.getAcquisition().before(start) && (d.getRelease() == null || d.getRelease().after(end))) {
					summary.get("retained").add(d);
				}
			}
		}
		return summary;
	}
	
	/* end device methods */

	/* start person methods */
	/**
	 * @param terminated if null, include everyone; if true, include only terminated people; if false, include only active (i.e. non-terminated) people
	 * @return list of people
	 */
	public List<Person> getPersons(Boolean terminated) {
		return repository.getPersons(terminated);
	}

	public boolean hasPersons() {
		return repository.getPersonCount() > 0;
	}

	public Person getPerson(Long id) {
		return repository.get(Person.class, id);
	}
	
	public PersonCredential getPersonCredential(String username) {
		return repository.getPersonCredential(username);
	}

	@Transactional(readOnly=false)
	public void setPassword(PersonCredential credential, String newPassword) {
		credential.setPassword(passwordEncoder.encode(newPassword));
		repository.save(credential);
	}
	
	/**
	 * Cannot have @PreAuthorize(role=user) because data seeder needs to call this.
	 * @param person to be added
	 * @return newly persisted person
	 */
	@Transactional(readOnly=false)
	public Person addPerson(Person person) {
		person.setCreated(new Date());
		repository.add(person);
		return person;
	}

	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#person, 'WRITE')")
	public void savePerson(Person person) {
		repository.save(person);
	}

	public PersonCredential savePersonCredential(PersonCredential credential) {
		repository.add(credential);
		return credential;
	}


	@Transactional(readOnly=false)
	@PreAuthorize("hasPermission(#person, 'DELETE')")
	public void deletePerson(Person person) {
		repository.delete(person);
	}
	
	/* end person methods */

	/**
	 * @param address, possibly with personal part
	 * @return email address, trimmed, lowercase, no personal part
	 */
	public static String normalizeEmailAddress(String address) {
		String normalized = null;
		if (StringUtils.hasText(address)) {
			address = address.toLowerCase();
			try {
				InternetAddress ia = new InternetAddress(address);
				normalized = ia.getAddress();
			} catch (AddressException e) {
				LOGGER.info("problem parsing email address: " + address);
			}
		}
		return normalized;
	}
}
