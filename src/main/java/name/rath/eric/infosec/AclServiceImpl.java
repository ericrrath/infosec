/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import name.rath.eric.infosec.domain.Device;
import name.rath.eric.infosec.domain.DeviceAudit;
import name.rath.eric.infosec.domain.Person;
import name.rath.eric.infosec.domain.Policy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.AccessControlEntryImpl;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.domain.CumulativePermission;
import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.Acl;
import org.springframework.security.acls.model.AclService;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;
import org.springframework.stereotype.Component;

/**
 * AclServiceImpl retrieves/generates Acl instances.
 */
@Component
public class AclServiceImpl implements AclService {
	@Autowired
	Repository repository;
	
	/**
	 * Access is currently granted to roles, not to individuals.
	 * @return <code>Acl</code> owned by the first <code>PrincipalSid</code> in <code>sids</code>,
	 * with one <code>AccessControlEntry</code> for each <code>GrantedAuthoritySid</code> in 
	 * <code>sids</code>.
	 */
	@Override
	public Acl readAclById(ObjectIdentity objectIdentity, List<Sid> sids) throws NotFoundException {
		PrincipalSid owner = null;
		List<GrantedAuthoritySid> grantedAuthoritySids = new ArrayList<GrantedAuthoritySid>();
		for (Sid sid: sids) {
			if (sid instanceof GrantedAuthoritySid) {
				grantedAuthoritySids.add((GrantedAuthoritySid)sid);
			} else if (sid instanceof PrincipalSid && owner == null) {
				owner = (PrincipalSid)sid;
			}
		}
		AclImpl acl = new AclImpl(objectIdentity, owner);
		
		Object object = null;
		try {
			Class<?> type = Class.forName(objectIdentity.getType());
			object = repository.get(type, (Long)objectIdentity.getIdentifier());
		} catch (ClassNotFoundException | EntityNotFoundException e) {
			throw new NotFoundException(e.getMessage());
		}
		Permission permission = getPermission(object);
		
		for (GrantedAuthoritySid gas: grantedAuthoritySids) {
			acl.getEntries().add(new AccessControlEntryImpl(null, acl, gas, permission, true, true, true));
		}
		return acl;
	}

	/**
	 * Loads object, examines, and determines permission.
	 * @param objectIdentity
	 * @return permissions on object
	 */
	static CumulativePermission getPermission(Object object) {
		CumulativePermission permission = new CumulativePermission();
		
		//TODO: define ACL logic for all types on which perms will be asserted
		if (object instanceof Policy) {
			Policy policy = (Policy)object;
			permission.set(BasePermission.READ);
			permission.set(BasePermission.WRITE);
			if (policy.getRequests().isEmpty()) {
				permission.set(BasePermission.DELETE);
			}
		} else if (object instanceof Device) {
			Device device = (Device)object;
			permission.set(BasePermission.READ);
			permission.set(BasePermission.WRITE);
			//only grant delete if device has no audits
			//NOTE: this may cause excessive queries 
			if (device.getAudits().isEmpty()) {
				permission.set(BasePermission.DELETE);
			}
			//only grant release if device hasn't been released (assert create to add audits or assignments)
			if (device.getRelease() == null) {
				permission.set(BasePermission.CREATE);
			}
		} else if (object instanceof DeviceAudit) {
			DeviceAudit audit = (DeviceAudit)object;
			permission.set(BasePermission.READ);
			//only grant write and delete on audits less than one week old
			if (System.currentTimeMillis() - audit.getAuditDate().getTime() < 1000 * 60 * 60 * 24 * 7) {
				permission.set(BasePermission.WRITE);
				permission.set(BasePermission.DELETE);
			}
		} else if (object instanceof Person) {
			Person person = (Person)object;
			permission.set(BasePermission.READ);
			permission.set(BasePermission.WRITE);
			//only grant delete if person has no assignments (even released ones)
			//NOTE: this may cause excessive queries 
			if (person.getAssignments().isEmpty()) {
				permission.set(BasePermission.DELETE);
			}
		} else {
			//safety - no perms if we don't know what it is
			permission.clear();
		}
		return permission;
	}

	/**
	 * @throws UnsupportedOperationException always
	 */
	@Override
	public List<ObjectIdentity> findChildren(ObjectIdentity arg0) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @throws UnsupportedOperationException always
	 */
	@Override
	public Acl readAclById(ObjectIdentity arg0) throws NotFoundException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @throws UnsupportedOperationException always
	 */
	@Override
	public Map<ObjectIdentity, Acl> readAclsById(List<ObjectIdentity> arg0) throws NotFoundException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @throws UnsupportedOperationException always
	 */
	@Override
	public Map<ObjectIdentity, Acl> readAclsById(List<ObjectIdentity> arg0, List<Sid> arg1) throws NotFoundException {
		throw new UnsupportedOperationException();
	}
}
