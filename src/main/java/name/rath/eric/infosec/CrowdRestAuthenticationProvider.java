/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.util.StringUtils;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * CrowdRestAuthenticationProvider delegates authentication of username/password
 * to an Atlassian Crowd instance via the REST API.
 */
public class CrowdRestAuthenticationProvider implements AuthenticationProvider {
	private static final Logger LOGGER = LoggerFactory.getLogger(CrowdRestAuthenticationProvider.class);
	private final URI baseUri;
	private final HttpHeaders httpHeaders;
	private final Collection<GrantedAuthority> defaultAuthorities = new HashSet<>();
	private final ResponseErrorHandler errorHandler = new ResponseErrorHandlerImpl();

	/**
	 * @param uriString location of Crowd authentication endpoint
	 * @param username used to authenticate app with Crowd
	 * @param password used to authenticate app with Crowd
	 */
	public CrowdRestAuthenticationProvider(String uriString, String username, String password) {
		URI uri = null;
		if (StringUtils.hasText(uriString)) {
			try {
				uri = new URI(uriString);
			} catch (URISyntaxException e) {
				uri = null;
				LOGGER.warn("bad URI: {} -- Crowd authentication disabled", uriString);
			}
		}
		baseUri = uri;

		String auth = username + ":" + password;
		byte[] encodedAuth = Base64.encode(auth.getBytes(Charset.forName("US-ASCII")));
		httpHeaders = new HttpHeaders();
		httpHeaders.add("Authorization", "Basic " + new String(encodedAuth, Charset.forName("US-ASCII")));
		//TODO: switch from sending and receiving XML to JSON
		httpHeaders.setContentType(MediaType.APPLICATION_XML);
		List<MediaType> accept = new ArrayList<>();
		accept.add(MediaType.APPLICATION_XML);
		httpHeaders.setAccept(accept);

		defaultAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		if (baseUri != null) {
			LOGGER.info("Crowd auth provider base URI: {}", baseUri);
		} else {
			LOGGER.warn("Crowd auth provider not enabled");
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (baseUri != null) && (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;
		
		if (!passesAuthentication(token.getName(), token.getCredentials().toString())) {
			throw new BadCredentialsException("Authentication failed for " + authentication.getPrincipal().toString());
		}
		
		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(
				token.getName(), authentication.getCredentials(),
				defaultAuthorities);

		return result;
	}

	public boolean passesAuthentication(String username, String password) {
		LOGGER.debug("checking username={}", username);
		
		RestTemplate rt = new RestTemplate();
		rt.setErrorHandler(errorHandler);
		HttpEntity<String> httpEntity = new HttpEntity<>(buildPasswordDocument(password), httpHeaders);
		ResponseEntity<String> response = rt.postForEntity(buildAuthenticationUri(username), httpEntity, String.class);
		
		LOGGER.debug("response code: {}", response.getStatusCode().value());
		
		return HttpStatus.OK.equals(response.getStatusCode());
	}

	/**
	 * @param username to be authenticated
	 * @return URI to which POST request will be sent
	 */
	public URI buildAuthenticationUri(String username) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUri(baseUri);
		//TODO: encode username value
		uriBuilder.queryParam("username", username);
		return uriBuilder.build().toUri();
	}

	/**
	 * @param password encoded as necessary
	 * @return XML document in format expected by Crowd 
	 */
	protected String buildPasswordDocument(String password) {
		StringWriter writer = new StringWriter();
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			doc.setXmlStandalone(true);
			Element rootElement = doc.createElement("password");
			doc.appendChild(rootElement);

			Element value = doc.createElement("value");
			rootElement.appendChild(value);
			value.appendChild(doc.createTextNode(password));
			
			DOMSource domSource = new DOMSource(doc);
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
		} catch (ParserConfigurationException | TransformerException e) {
			LOGGER.error("couldn't build password doc for {}", password);
		}
		LOGGER.debug("built password document: {}", writer);
		return writer.toString();
	}

	/**
	 * ResponseErrorHandlerImpl considers HttpStatus.BAD_REQUEST to *not*
	 * be an error; Crowd API call to authenticate returns 400 if
	 * authentication fails.
	 */
	static class ResponseErrorHandlerImpl extends DefaultResponseErrorHandler {
		@Override
		protected boolean hasError(HttpStatus statusCode) {
			return (statusCode.series() == HttpStatus.Series.SERVER_ERROR ||
					(statusCode.series() == HttpStatus.Series.CLIENT_ERROR
					&& !statusCode.equals(HttpStatus.BAD_REQUEST)));
		}
	}
}
