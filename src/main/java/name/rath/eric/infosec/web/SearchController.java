/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import java.util.Optional;

import org.elasticsearch.action.search.SearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import name.rath.eric.infosec.InfoSecService;
import name.rath.eric.infosec.SearchDao;
import name.rath.eric.infosec.domain.Device;
import name.rath.eric.infosec.domain.DeviceAudit;

@Controller
@RequestMapping("/search")
public class SearchController {
	@Autowired
	protected SearchDao dao;

	@Autowired
	protected InfoSecService service;

	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView get(@RequestParam(required=false) String term) {
		ModelAndView mav = new ModelAndView("searchView");
		if (StringUtils.hasText(term)) {
			Optional<SearchResponse> response = dao.search(term);
			if (response.isPresent()) {
				mav.addObject("searchResponse", response.get());
			}
		}
		return mav;
	}

	//TODO: remove this, or replace with more scalable implementation; at least use async to run on non-req-processing-thread
	@RequestMapping(value="/reindex", method=RequestMethod.GET)
	public String reindex() {
		for (Device d: service.getDevices(null)) {
			dao.add(d);
			for (DeviceAudit da: d.getAudits()) {
				dao.add(da);
			}
		}
		return "redirect:/search";
	}
}
