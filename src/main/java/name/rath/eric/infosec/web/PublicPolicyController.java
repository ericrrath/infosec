/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import javax.servlet.http.HttpServletRequest;

import name.rath.eric.infosec.InfoSecService;
import name.rath.eric.infosec.domain.PolicyRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/public/policies")
public class PublicPolicyController {
	InfoSecService service;

	@Autowired
	public PublicPolicyController(InfoSecService service) {
		this.service = service;
	}
	
	@InitBinder
	public void setAllowedFields(WebDataBinder binder) {
		binder.setDisallowedFields("id");
	}
	
	@RequestMapping(value="/request", method=RequestMethod.GET)
	public ModelAndView view(@RequestParam("code") String code) {
		PolicyRequest policyRequest = service.getPolicyRequest(code);
		ModelAndView mav = new ModelAndView("publicViewRequest", "policyRequest", policyRequest);
		if (policyRequest != null) {
			mav.addObject("bodyMarkup", service.getPolicyRendered(policyRequest.getPolicy()));
		}
		return mav;
	}
	
	@RequestMapping(value="/request", method=RequestMethod.POST)
	public String respond(HttpServletRequest request, @RequestParam("code") String code, @RequestParam("accept") boolean accept) {
		PolicyRequest policyRequest = service.getPolicyRequest(code);
		if (policyRequest != null) {
			service.savePolicyResponse(policyRequest, request.getRemoteAddr(), accept);
			return "redirect:/public/policies/request?code=" + code;
		}
		//TODO: send response with link to bookmark for future viewing of policy
		return "redirect:/";
	}
}
