/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import name.rath.eric.infosec.InfoSecService;
import name.rath.eric.infosec.domain.Person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/persons")
public class PersonController {
	InfoSecService service;

	@Autowired
	public PersonController(InfoSecService service) {
		this.service = service;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView index(@RequestParam(required=false) Boolean terminated) {
		return new ModelAndView("persons", "persons", service.getPersons(terminated));
	}
	
	@RequestMapping(value="/{personId:\\d+}", method=RequestMethod.GET)
	public ModelAndView view(@PathVariable("personId") Long id) {
		Person person = service.getPerson(id);
		return new ModelAndView("person", "person", person);
	}
}
