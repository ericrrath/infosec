/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import name.rath.eric.infosec.InfoSecService;
import name.rath.eric.infosec.domain.Policy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/policies")
@SessionAttributes("policy")
public class PolicyController {
	InfoSecService service;

	@Autowired
	public PolicyController(InfoSecService service) {
		this.service = service;
	}
	
	@InitBinder
	public void setAllowedFields(WebDataBinder binder) {
		binder.setDisallowedFields("id");
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView index() {
		return new ModelAndView("policies", "policies", service.getPolicies());
	}
	
	@RequestMapping(value="/{policyId:\\d+}", method=RequestMethod.GET)
	public ModelAndView view(@PathVariable("policyId") Long id) {
		Policy policy = service.getPolicy(id);
		ModelAndView mav = new ModelAndView("policy", "policy", policy);
		mav.addObject("bodyMarkup", service.getPolicyRendered(policy));
		return mav;
	}
	
	@RequestMapping(value="/{policyId:\\d+}/edit", method=RequestMethod.GET)
	public String getEditForm(@PathVariable("policyId") Long id, Model model) {
		Policy policy = service.getPolicy(id);
		model.addAttribute(policy);
		return "policyForm";
	}
	
	@RequestMapping(value="/{policyId:\\d+}/edit", method=RequestMethod.POST)
	public String putEditForm(@ModelAttribute("policy") @Valid Policy policy, BindingResult result, SessionStatus status) {
		if (result.hasErrors()) {
			return "policyForm";
		}
		policy = service.savePolicy(policy);
		status.setComplete();
		return "redirect:/policies/" + policy.getId();
	}

	@RequestMapping(value="/{policyId:\\d+}/edit", method=RequestMethod.POST, params="_cancel")
	public String cancelEditForm(@ModelAttribute("policy") Policy policy, SessionStatus status) {
		status.setComplete();
		return "redirect:/policies/" + policy.getId();
	}
	
	@RequestMapping(value="/new", method=RequestMethod.GET)
	public String getNewPolicyForm(Model model) {
		Policy policy = new Policy();
		policy.setModified(new Date());
		model.addAttribute(policy);
		return "policyForm";
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public String postNewPolicyForm(@ModelAttribute("policy") @Valid Policy policy, BindingResult result, SessionStatus status) {
		if (result.hasErrors()) {
			return "policyForm";
		}
		policy = service.addPolicy(policy);
		status.setComplete();
		return "redirect:/policies/" + policy.getId();
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST, params="_cancel")
	public String addDelete() {
		return "redirect:/policies/";
	}

	
	@RequestMapping(value="/{policyId:\\d+}/sendRequest", method=RequestMethod.GET)
	public ModelAndView viewSendRequestForm(@PathVariable("policyId") Long id) {
		Policy policy = service.getPolicy(id);
		ModelAndView mav = new ModelAndView("policySendRequestForm", "policy", policy);
		return mav;
	}

	@RequestMapping(value="/{policyId:\\d+}/sendRequest", method=RequestMethod.POST)
	public ModelAndView sendRequestForm(@PathVariable("policyId") Long id, @RequestParam("addresses") String addresses) throws IOException {
		Policy policy = service.getPolicy(id);
		Set<String> emailAddresses = new HashSet<String>();
		BufferedReader br = new BufferedReader(new StringReader(addresses));
		String line = null;
		while ((line = br.readLine()) != null) {
			String[] lineAddresses = line.split(",");
			for (String addr: lineAddresses) {
				if (StringUtils.hasText(addr)) {
					//TODO: validate email address
					emailAddresses.add(addr.trim().toLowerCase());
				}
			}
		}
		service.sendPolicyRequests(policy, emailAddresses);
		return new ModelAndView("redirect:/policies/" + policy.getId());
	}
	
	@RequestMapping(value="/{policyId:\\d+}/delete", method=RequestMethod.GET)
	public ModelAndView confirmDelete(@PathVariable("policyId") Long id) {
		return new ModelAndView("policyConfirmDelete", "policy", service.getPolicy(id));
	}

	@RequestMapping(value="/{policyId:\\d+}/delete", method=RequestMethod.POST)
	public String delete(@PathVariable("policyId") Long id) {
		Policy p = service.getPolicy(id);
		service.deletePolicy(p);
		return "redirect:/policies/";
	}

	@RequestMapping(value="/{policyId:\\d+}/delete", method=RequestMethod.POST, params="_cancel")
	public String cancelDelete() {
		return "redirect:/policies/";
	}
}
