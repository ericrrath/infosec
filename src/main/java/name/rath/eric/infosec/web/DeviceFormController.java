/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import javax.validation.Valid;

import name.rath.eric.infosec.InfoSecService;
import name.rath.eric.infosec.domain.Device;
import name.rath.eric.infosec.domain.DeviceAssignment;
import name.rath.eric.infosec.domain.Person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/devices")
@SessionAttributes("device")
public class DeviceFormController {
	final InfoSecService service;

	@Autowired
	public DeviceFormController(InfoSecService service) {
		this.service = service;
	}

	@InitBinder
	public void setAllowedFields(WebDataBinder binder) {
		binder.setDisallowedFields("id");
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	@ModelAttribute
	public void referenceData(Model model) {
		model.addAttribute("names", service.getDeviceNames());
	}

	@RequestMapping(value="/new", method=RequestMethod.GET)
	public String newFormGet(Model model) {
		Device device = new Device();
		model.addAttribute(device);
		return "deviceForm";
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public String newFormPost(@ModelAttribute("device") @Valid Device device, BindingResult result, SessionStatus status) {
		if (result.hasErrors()) {
			return "deviceForm";
		}
		service.addDevice(device);
		return newFormPostDone(status);
	}

	@RequestMapping(value="/new", method=RequestMethod.POST, params="_cancel")
	public String newFormPostDone(SessionStatus status) {
		status.setComplete();
		return "redirect:/devices/";
	}

	@RequestMapping(value="/{deviceId:\\d+}/edit", method=RequestMethod.GET)
	public String editFormGet(@PathVariable("deviceId") Long id, Model model) {
		Device device = service.getDevice(id);
		model.addAttribute(device);
		return "deviceForm";
	}

	@RequestMapping(value="/{deviceId:\\d+}/edit", method=RequestMethod.POST)
	public String editFormPost(@ModelAttribute("device") @Valid Device device, BindingResult result, SessionStatus status) {
		//TODO: validate release date after acquisition
		if (result.hasErrors()) {
			return "deviceForm";
		}
		device = service.saveDevice(device);
		return editFormPostDone(device.getId(), status);
	}

	@RequestMapping(value="/{deviceId:\\d+}/edit", method=RequestMethod.POST, params="_cancel")
	public String editFormPostDone(@PathVariable("deviceId") Long deviceId, SessionStatus status) {
		status.setComplete();
		return "redirect:/devices/" + deviceId;
	}

	@RequestMapping(value="/{deviceId:\\d+}/assign", method=RequestMethod.GET)
	public ModelAndView assignFormGet(@PathVariable("deviceId") Long deviceId) {
		ModelAndView mav = new ModelAndView("deviceAssignForm");
		mav.addObject(service.getDevice(deviceId));
		mav.addObject("persons", service.getPersons(false));
		return mav;
	}

	@RequestMapping(value="/{deviceId:\\d+}/assign", method=RequestMethod.POST)
	public String assignFormPost(@PathVariable("deviceId") Long deviceId, @RequestParam("personId") Long personId) {
		Device device = service.getDevice(deviceId);
		Person person = service.getPerson(personId);
		service.assign(device, person);
		return assignFormPostDone(deviceId);
	}

	@RequestMapping(value="/{deviceId:\\d+}/assign", method=RequestMethod.POST, params="_cancel")
	public String assignFormPostDone(@PathVariable("deviceId") Long deviceId) {
		return "redirect:/devices/" + deviceId;
	}

	@RequestMapping(value="/{deviceId:\\d+}/release", method=RequestMethod.GET)
	public String release(@PathVariable("deviceId") Long deviceId, @RequestParam("assignmentId") Long assignmentId) {
		Device device = service.getDevice(deviceId);
		DeviceAssignment assignment = service.getDeviceAssignment(assignmentId);
		service.release(assignment);
		return "redirect:/devices/" + device.getId();
	}

	@RequestMapping(value="/{deviceId:\\d+}/delete", method=RequestMethod.GET)
	public ModelAndView deleteGet(@PathVariable("deviceId") Long id) {
		return new ModelAndView("deviceConfirmDelete", "device", service.getDevice(id));
	}

	@RequestMapping(value="/{deviceId:\\d+}/delete", method=RequestMethod.POST)
	public String deletePost(@PathVariable("deviceId") Long id) {
		service.deleteDevice(service.getDevice(id));
		return "redirect:/devices/";
	}

	@RequestMapping(value="/{deviceId:\\d+}/delete", method=RequestMethod.POST, params="_cancel")
	public String deleteCancel(@PathVariable("deviceId") Long id) {
		return String.format("redirect:/devices/%d", id);
	}
}
