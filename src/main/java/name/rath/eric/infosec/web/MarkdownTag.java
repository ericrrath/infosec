/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import java.io.IOException;

import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TryCatchFinally;

import org.pegdown.PegDownProcessor;

/**
 * MarkdownTag renders input as Markdown.
 */
//TODO: support caching of rendered values?
public class MarkdownTag extends SimpleTagSupport implements TryCatchFinally {
	private String value;

	@Override
	public void doTag() throws IOException {
		getJspContext().getOut().append(new PegDownProcessor().markdownToHtml(value));
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public void doCatch(Throwable t) throws Throwable {
		t.printStackTrace();
	}

	@Override
	public void doFinally() {
		value = null;
	}
}
