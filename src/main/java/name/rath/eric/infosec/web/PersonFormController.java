/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import java.util.Date;

import name.rath.eric.infosec.InfoSecService;
import name.rath.eric.infosec.domain.Person;
import name.rath.eric.infosec.domain.valid.Web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@RequestMapping("/persons")
@SessionAttributes("person")
public class PersonFormController {
	InfoSecService service;

	@Autowired
	public PersonFormController(InfoSecService service) {
		this.service = service;
	}

	@InitBinder
	public void setAllowedFields(WebDataBinder binder) {
		binder.setDisallowedFields("id");
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@RequestMapping(value="/new", method=RequestMethod.GET)
	public String getAddForm(Model model) {
		Person person = new Person();
		person.setCreated(new Date());
		model.addAttribute(person);
		return "personForm";
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public String postAddForm(@ModelAttribute("person") @Validated(Web.class) Person person, BindingResult result, SessionStatus status) {
		if (result.hasErrors()) {
			return "personForm";
		}
		service.addPerson(person);
		status.setComplete();
		return "redirect:/persons/" + person.getId();
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST, params="_cancel")
	public String cancelAddForm() {
		return "redirect:/persons";
	}
	
	@RequestMapping(value="/{personId:\\d+}/edit", method=RequestMethod.GET)
	public String getEditForm(@PathVariable("personId") Long id, Model model) {
		Person person = service.getPerson(id);
		model.addAttribute(person);
		return "personForm";
	}
	
	@RequestMapping(value="/{personId:\\d+}/edit", method=RequestMethod.POST)
	public String postEditForm(@ModelAttribute("person") @Validated(Web.class) Person person, BindingResult result, SessionStatus status) {
		if (result.hasErrors()) {
			return "personForm";
		}
		service.savePerson(person);
		status.setComplete();
		return "redirect:/persons/" + person.getId();
	}

	@RequestMapping(value="/{personId:\\d+}/edit", method=RequestMethod.POST, params="_cancel")
	public String cancelEditForm(@PathVariable("personId") Long id, SessionStatus status) {
		status.setComplete();
		return "redirect:/persons/" + id;
	}
	
	@RequestMapping(value="/{personId:\\d+}/delete", method=RequestMethod.GET)
	public String getDeleteForm(@PathVariable("personId") Long id, Model model) {
		Person person = service.getPerson(id);
		model.addAttribute(person);
		return "personConfirmDelete";
	}
	
	@RequestMapping(value="/{personId:\\d+}/delete", method=RequestMethod.POST)
	public String postDeleteForm(@PathVariable("personId") Long id, SessionStatus status) {
		service.deletePerson(service.getPerson(id));
		status.setComplete();
		return "redirect:/persons";
	}

	@RequestMapping(value="/{personId:\\d+}/delete", method=RequestMethod.POST, params="_cancel")
	public String cancelDeleteForm(@PathVariable("personId") Long id, SessionStatus status) {
		status.setComplete();
		return "redirect:/persons/" + id;
	}
}
