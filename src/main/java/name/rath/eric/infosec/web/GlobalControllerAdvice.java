/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.appsensor.core.DetectionPoint;
import org.owasp.appsensor.core.DetectionSystem;
import org.owasp.appsensor.core.Event;
import org.owasp.appsensor.core.User;
import org.owasp.appsensor.core.event.EventManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * GlobalControllerAdvice defines exception handlers for any 
 * exceptions not sufficiently handled by Spring's default behavior.
 */
@ControllerAdvice
public class GlobalControllerAdvice {
	private static final DetectionSystem DETECTION_SYSTEM = new DetectionSystem("infosec.global-controller-advice");

	private static final DetectionPoint DETECTION_POINT_RE9 = new DetectionPoint(DetectionPoint.Category.REQUEST, "RE9");

	private static final DetectionPoint DETECTION_POINT_ACE1 = new DetectionPoint(DetectionPoint.Category.ACCESS_CONTROL, "ACE1");

	@Autowired
	EventManager eventManager;


	/**
	 * Sets response status code to 404.
	 * @param request used to query for client IP address
	 * @param response used to send error
	 * @throws IOException if error cannot be sent
	 */
	@ExceptionHandler(ObjectRetrievalFailureException.class)
	public void handleNotFound(HttpServletRequest request, HttpServletResponse response) throws IOException {
		eventManager.addEvent(new Event(new User(request.getRemoteAddr()), DETECTION_POINT_ACE1, DETECTION_SYSTEM));
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
	
	/**
	 * Sets response status code to 404.
	 * @param request used to query for client IP address
	 * @param response used to send error
	 * @throws IOException if error cannot be sent
	 */
	@ExceptionHandler(NoHandlerFoundException.class)
	public void handleNoHandlerFound(HttpServletRequest request, HttpServletResponse response) throws IOException {
		eventManager.addEvent(new Event(new User(request.getRemoteAddr()), DETECTION_POINT_RE9, DETECTION_SYSTEM));
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}

}
