/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import java.io.IOException;
import java.util.Date;

import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TryCatchFinally;

import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

/**
 * DurationTag renders the duration of a period between two dates (e.g. "2y 5m").
 * Uses Joda because java.time doesn't provide Period formatter.
 */
public class DurationTag extends SimpleTagSupport implements TryCatchFinally {
	private final PeriodFormatter formatter;

	private Date start;
	private Date end;

	public DurationTag() {
		formatter = new PeriodFormatterBuilder()
			     .printZeroRarelyLast()
			     .appendYears()
			     .appendSuffix("y")
			     .appendSeparator(", ")
			     .appendMonths()
			     .appendSuffix("m")
			     .appendSeparator(", ")
			     .appendDays()
			     .appendSuffix("d")
			     .toFormatter();
	}

	@Override
	public void doTag() throws IOException {
		if (start != null) {
			Date effectiveEnd = (end != null) ? end : new Date();
			Period period = new Period(start.getTime(), effectiveEnd.getTime(), PeriodType.yearMonthDay());
			formatter.printTo(getJspContext().getOut(), period);
		}
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	@Override
	public void doCatch(Throwable t) throws Throwable {
		t.printStackTrace();
	}

	@Override
	public void doFinally() {
		start = null;
		end = null;
	}
}
