/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PasswordChange {
	@NotNull
	String current;
	
	@NotNull
	@Size(min=6)
	String newPassword;
	
	@NotNull
	@Size(min=6)
	String newPasswordConfirm;

	public String getCurrent() {
		return current;
	}
	public void setCurrent(String current) {
		this.current = current;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getNewPasswordConfirm() {
		return newPasswordConfirm;
	}
	public void setNewPasswordConfirm(String newPasswordConfirm) {
		this.newPasswordConfirm = newPasswordConfirm;
	}
}
