/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import java.util.Calendar;
import java.util.Date;

import name.rath.eric.infosec.InfoSecService;
import name.rath.eric.infosec.domain.Device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/devices")
public class DeviceController {
	final InfoSecService service;

	@Autowired
	public DeviceController(InfoSecService service) {
		this.service = service;
	}

	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView index(@RequestParam(required=false) Short typeId) {
		ModelAndView mav = new ModelAndView("devices", "devices", service.getDevices(Device.Type.get(typeId)));
		mav.addObject("auditDates", service.getMostRecentAuditDates());
		mav.addObject("assignments", service.getCurrentAssignments());
		return mav;
	}

	@RequestMapping(value="/summary", method=RequestMethod.GET)
	public ModelAndView summary(@RequestParam(value="start", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date rawStart,
			@RequestParam(value="end", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date rawEnd,
			@RequestParam(value="owned", required=false) Boolean owned) {
		ModelAndView mav = new ModelAndView("devicesSummary");
		Date start = rawStart;
		if (start == null) {
			Calendar c = Calendar.getInstance();
			c.add(Calendar.YEAR, -1);
			start = c.getTime();
		}
		Date end = rawEnd;
		if (end == null) {
			Calendar c = Calendar.getInstance();
			end = c.getTime();
		}
		mav.addObject("summary", service.getDeviceSummary(start, end, owned));
		mav.addObject("start", start);
		mav.addObject("end", end);
		mav.addObject("owned", owned);
		return mav;
	}

	@RequestMapping(value="/{deviceId:\\d+}", method=RequestMethod.GET)
	public ModelAndView view(@PathVariable("deviceId") Long id) {
		Device device = service.getDevice(id);
		return new ModelAndView("device", "device", device);
	}

	@RequestMapping(value="/{\\d+}/audits/{auditId:\\d+}", method=RequestMethod.GET)
	public ModelAndView auditView(@PathVariable("auditId") Long id) {
		return new ModelAndView("deviceAuditView", "audit", service.getDeviceAudit(id));
	}
}
