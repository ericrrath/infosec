/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import java.util.Date;

import name.rath.eric.infosec.InfoSecService;
import name.rath.eric.infosec.domain.Device;
import name.rath.eric.infosec.domain.DeviceAudit;
import name.rath.eric.infosec.domain.valid.Web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/devices/{deviceId:\\d+}/audits/")
@SessionAttributes("deviceAudit")
public class DeviceAuditFormController {
	final InfoSecService service;

	@Autowired
	public DeviceAuditFormController(InfoSecService service) {
		this.service = service;
	}

	@InitBinder
	public void setAllowedFields(WebDataBinder binder) {
		binder.setAllowedFields("notes", "auditDate");
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@RequestMapping(value="/new", method=RequestMethod.GET)
	public ModelAndView newFormGet(@PathVariable("deviceId") Long deviceId) {
		ModelAndView mav = new ModelAndView("deviceAuditForm");
		DeviceAudit audit = new DeviceAudit();
		audit.setAuditDate(new Date());
		mav.addObject(audit);
		mav.addObject(service.getDevice(deviceId));
		return mav;
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public String newFormPost(@PathVariable("deviceId") Long deviceId, @ModelAttribute("deviceAudit") @Validated(Web.class) DeviceAudit audit, BindingResult result, SessionStatus status) {
		if (result.hasErrors()) {
			return "deviceAuditForm";
		}
		Device device = service.getDevice(deviceId);
		service.addDeviceAudit(device, audit);
		return newFormPostDone(device.getId(), status);
	}

	@RequestMapping(value="/new", method=RequestMethod.POST, params="_cancel")
	public String newFormPostDone(@PathVariable("deviceId") Long deviceId, SessionStatus status) {
		status.setComplete();
		return "redirect:/devices/" + deviceId;
	}

	@RequestMapping(value="/{auditId:\\d+}/edit", method=RequestMethod.GET)
	public ModelAndView editFormGet(@PathVariable("auditId") Long auditId) {
		ModelAndView mav = new ModelAndView("deviceAuditForm");
		DeviceAudit audit = service.getDeviceAudit(auditId);
		mav.addObject(audit);
		mav.addObject(audit.getDevice());
		return mav;
	}

	@RequestMapping(value="/{auditId:\\d+}/edit", method=RequestMethod.POST)
	public String editFormPost(@PathVariable("auditId") Long auditId, @ModelAttribute("deviceAudit") @Validated(Web.class) DeviceAudit audit, BindingResult result, SessionStatus status) {
		if (result.hasErrors()) {
			return "deviceAuditForm";
		}
		service.saveDeviceAudit(audit);
		return editFormPostDone(audit.getDevice().getId(), audit.getId(), status);
	}
	
	@RequestMapping(value="/{auditId:\\d+}/edit", method=RequestMethod.POST, params="_cancel")
	public String editFormPostDone(@PathVariable("deviceId") Long deviceId, @PathVariable("auditId") Long auditId, SessionStatus status) {
		status.setComplete();
		return String.format("redirect:/devices/%d/audits/%d", deviceId, auditId);
	}
	
	@RequestMapping(value="/{auditId:\\d+}/delete", method=RequestMethod.GET)
	public ModelAndView deleteGet(@PathVariable("auditId") Long id) {
		ModelAndView mav = new ModelAndView("deviceAuditConfirmDelete");
		DeviceAudit audit = service.getDeviceAudit(id);
		mav.addObject(audit);
		mav.addObject(audit.getDevice());
		return mav;
	}

	@RequestMapping(value="/{auditId:\\d+}/delete", method=RequestMethod.POST)
	public String deletePost(@PathVariable("deviceId") Long deviceId, @PathVariable("auditId") Long auditId) {
		service.deleteDeviceAudit(service.getDeviceAudit(auditId));
		return String.format("redirect:/devices/%d", deviceId);
	}

	@RequestMapping(value="/{auditId:\\d+}/delete", method=RequestMethod.POST, params="_cancel")
	public String deleteCancel(@PathVariable("deviceId") Long deviceId, @PathVariable("auditId") Long auditId) {
		return String.format("redirect:/devices/%d/audits/%d", deviceId, auditId);
	}
}
