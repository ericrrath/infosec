/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.web;

import javax.validation.Valid;

import name.rath.eric.infosec.InfoSecService;
import name.rath.eric.infosec.domain.PersonCredential;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@RequestMapping("/changePassword")
public class ChangePasswordFormController {
	InfoSecService service;
	PasswordEncoder passwordEncoder;
	
	@Autowired
	public ChangePasswordFormController(InfoSecService service, PasswordEncoder passwordEncoder) {
		this.service = service;
		this.passwordEncoder = passwordEncoder;
	}

	@InitBinder
	public void setAllowedFields(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@RequestMapping(method=RequestMethod.GET)
	public String get(ModelMap model) {
		model.addAttribute(new PasswordChange());
		return "changePasswordForm";
	}

	@RequestMapping(method=RequestMethod.POST)
	public String post(@ModelAttribute("passwordChange") @Valid PasswordChange change, BindingResult result, SessionStatus status) {
		SecurityContext context = SecurityContextHolder.getContext();
		Assert.notNull(context);
		PersonCredential credential = service.getPersonCredential(context.getAuthentication().getName());
		if (!result.hasErrors()) {
			if (!passwordEncoder.matches(change.getCurrent(), credential.getPassword())) {
				result.rejectValue("current", "person.password.current-invalid", "current password incorrect");
			}
			if (!change.getNewPassword().equals(change.getNewPasswordConfirm())) {
				result.rejectValue("newPasswordConfirm", "person.password.new-unequal", "new passwords must match");
			}
		}
		if (result.hasErrors()) {
			return "changePasswordForm";
		}
		service.setPassword(credential, change.getNewPassword());
		status.setComplete();
		return "redirect:/";
	}
	
	@RequestMapping(method=RequestMethod.POST, params="_cancel")
	public String cancel() {
		return "redirect:/";
	}
}
