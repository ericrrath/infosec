/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import org.owasp.appsensor.core.DetectionPoint;
import org.owasp.appsensor.core.DetectionSystem;
import org.owasp.appsensor.core.Event;
import org.owasp.appsensor.core.User;
import org.owasp.appsensor.core.event.EventManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

/**
 * AuthenticationEventListener reports authentication failures to AppSensor.
 */
@Component
public class AuthenticationEventListener implements ApplicationListener<AbstractAuthenticationEvent> {
	private static final DetectionSystem DETECTION_SYSTEM = new DetectionSystem("infosec.auth-event-listener");
	private static final DetectionPoint DETECTION_POINT_AE3 = new DetectionPoint(DetectionPoint.Category.AUTHENTICATION, "AE3");

	@Autowired
	protected EventManager eventManager;

	@Override
	public void onApplicationEvent(AbstractAuthenticationEvent event) {
		if (event instanceof AuthenticationFailureBadCredentialsEvent) {
			eventManager.addEvent(new Event(new User(event.getAuthentication().getName()), DETECTION_POINT_AE3, DETECTION_SYSTEM));
		}
	}
}
