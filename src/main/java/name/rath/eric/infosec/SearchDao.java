/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PreDestroy;

import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import name.rath.eric.infosec.domain.Device;
import name.rath.eric.infosec.domain.DeviceAudit;

@Component
public class SearchDao implements ApplicationListener<ContextRefreshedEvent> {
	private static final Logger LOGGER = LoggerFactory.getLogger(SearchDao.class);

	private String host;
	private int port;
	private String indexName;

	private TransportClient client;

	@Value("${search.endpoint}")
	public void setEndpoint(String endpoint) {
		Pattern p = Pattern.compile("^(.*?):(\\d+)/(.*?)$");
		Matcher m = p.matcher(endpoint);
		if (m.matches()) {
			host = m.group(1);
			port = Integer.parseInt(m.group(2));
			indexName = m.group(3);
		} else {
			throw new IllegalArgumentException("endpoint: host:port/indexName");
		}
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (client == null) {
			try {
				client = TransportClient.builder().build()
				        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(host), port));
				IndicesExistsResponse response = client.admin().indices().prepareExists(indexName).get();
				if (response.isExists()) {
					LOGGER.info("using index {}", indexName);
				} else {
					LOGGER.error("index \"{}\" doesn't exist", indexName);
					client = null;
				}
			} catch (UnknownHostException e) {
				LOGGER.error("problem creating search client", e);
				client = null;
			} catch (NoNodeAvailableException e) {
				LOGGER.error("no ElasticSearch nodes available");
				client = null;
			}
		}
	}
	
	public void add(Device device) {
		IndexRequest request = new IndexRequest(indexName, "device", device.getId().toString());
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode dataTable = mapper.createObjectNode();
		dataTable.put("id", device.getId());
		dataTable.put("name", device.getName());
		dataTable.put("description", device.getDescription());
		dataTable.put("serial-number", device.getSerialNumber());
		internalAdd(request, dataTable.toString());
	}

	public void add(DeviceAudit audit) {
		IndexRequest request = new IndexRequest(indexName, "audit", audit.getId().toString());
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode dataTable = mapper.createObjectNode();
		dataTable.put("id", audit.getId());
		dataTable.put("device-id", audit.getDevice().getId());
		dataTable.put("notes", audit.getNotes());
		internalAdd(request, dataTable.toString());
	}

	public void internalAdd(IndexRequest request, String body) {
		if (client == null) {
			LOGGER.warn("no search client available; ignoring request: {}", request);
			return;
		}
		request.source(body);
		IndexResponse response = client.index(request).actionGet();
		LOGGER.debug("added to index: {}", response.toString());
	}

	public Optional<SearchResponse> search(String term) {
		if (client == null) {
			return Optional.ofNullable(null);
		}
		SearchResponse response = client.prepareSearch(indexName)
		        .setQuery(QueryBuilders.multiMatchQuery(term, "name", "description", "serial-number", "notes"))
		        .addField("device-id").addField("name").addField("serial-number")
		        .addHighlightedField("description").addHighlightedField("notes")
		        .setHighlighterPreTags("<strong>").setHighlighterPostTags("</strong>")
		        .execute()
		        .actionGet();
		return Optional.of(response);
	}

	@PreDestroy
	public void preDestroy() {
		if (client != null) {
			client.close();
			LOGGER.warn("closed search client");
		}
	}
}
