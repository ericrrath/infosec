/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import name.rath.eric.infosec.domain.Person;
import name.rath.eric.infosec.domain.PersonCredential;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * DataSeeder creates default credentials upon context-refresh (i.e. startup) if none found in database
 */
@Component
public class DataSeeder implements ApplicationListener<ContextRefreshedEvent> {
	private static final Logger LOGGER = LoggerFactory.getLogger(DataSeeder.class);
	@Autowired
	protected InfoSecService service;

	@Autowired
	protected PasswordEncoder passwordEncoder;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (!service.hasPersons()) {
			Person p = new Person();
			p.setFirstName("Default");
			p.setLastName("Person");
			p = service.addPerson(p);
			LOGGER.info("created seed person: " + p);
			
			PersonCredential pc = new PersonCredential();
			pc.setPersonId(p.getId());
			pc.setUserName("admin");
			pc.setPassword(passwordEncoder.encode("infosec"));
			service.savePersonCredential(pc);
			LOGGER.info(String.format("created seed credentials: username=%s, password=%s", "admin", "infosec"));
		}
	}
}
