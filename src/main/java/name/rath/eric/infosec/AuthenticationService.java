/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.NoResultException;

import name.rath.eric.infosec.domain.PersonCredential;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * AuthenticationService facilitates authentication against list of people in database.
 */
@Service
public class AuthenticationService implements UserDetailsService {
	private static final Set<GrantedAuthority> DEFAULT_AUTHORITIES = new HashSet<GrantedAuthority>();
	static {
		DEFAULT_AUTHORITIES.add(new SimpleGrantedAuthority("ROLE_USER"));
		//authenticating against local db grants local role
		DEFAULT_AUTHORITIES.add(new SimpleGrantedAuthority("ROLE_LOCAL"));
	}

	@Autowired
	Repository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			PersonCredential p = repository.getPersonCredential(username);
			return new User(p.getUserName(), p.getPassword(), true, true, true, true, DEFAULT_AUTHORITIES);
		} catch (EmptyResultDataAccessException | NoResultException e) {
			throw new UsernameNotFoundException("not found: " + username);
		}
	}
}
