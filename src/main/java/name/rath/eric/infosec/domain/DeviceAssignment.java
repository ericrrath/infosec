/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.domain;

import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * NOTE: this class has a natural ordering that is inconsistent with equals.
 */
@Entity
@Table(name="device_assignment")
public class DeviceAssignment implements Comparable<DeviceAssignment> {
	protected static final Comparator<DeviceAssignment> COMPARATOR = Comparator.comparing(DeviceAssignment::getAssigned, Comparator.nullsLast(Date::compareTo).reversed())
			.thenComparing(DeviceAssignment::getReleased, Comparator.nullsLast(Date::compareTo).reversed())
			.thenComparing(DeviceAssignment::getId, Comparator.nullsFirst(Long::compareTo));

	@Id
	@Column(name="device_assignment_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="device_id")
	@NotNull
	private Device device;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="person_id")
	@NotNull
	private Person person;

	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	@DateTimeFormat(iso=ISO.DATE)
	private Date assigned;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(iso=ISO.DATE)
	private Date released;

	DeviceAssignment() {
		//no-op constructor needed for JPA?
	}
	
	/**
	 * <code>assigned</code> date is set to "now", <code>released</code> is left null.
	 * @param device to be assigned to person
	 * @param person accepting device
	 */
	public DeviceAssignment(Device device, Person person) {
		this.device = device;
		this.person = person;
		assigned = new Date();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Date getAssigned() {
		return assigned;
	}

	public void setAssigned(Date assigned) {
		this.assigned = assigned;
	}

	public Date getReleased() {
		return released;
	}

	public void setReleased(Date released) {
		this.released = released;
	}

	@Override
	public int compareTo(DeviceAssignment o) {
		return COMPARATOR.compare(this, o);
	}
}
