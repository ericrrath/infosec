/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.domain;

import java.util.Comparator;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortNatural;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import name.rath.eric.infosec.domain.valid.Web;

/**
 * NOTE: this class has a natural ordering that is inconsistent with equals.
 */
@Entity
@Table(name="person")
public class Person implements Comparable<Person> {
	protected static final Comparator<Person> COMPARATOR = Comparator.comparing(Person::getTermination, Comparator.nullsLast(Date::compareTo).reversed())
			.thenComparing(Person::getLastName, Comparator.nullsLast(String.CASE_INSENSITIVE_ORDER))
			.thenComparing(Person::getFirstName, Comparator.nullsLast(String.CASE_INSENSITIVE_ORDER))
			.thenComparing(Person::getId);

	@Id
	@Column(name="person_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(length=50)
	@NotNull(groups=Web.class)
	private String firstName;
	
	@Column(length=50)
	@NotNull(groups=Web.class)
	private String lastName;
	
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	@DateTimeFormat(iso=ISO.DATE)
	private Date created;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(iso=ISO.DATE)
	private Date termination;
	
	@OneToMany(mappedBy="person", cascade=CascadeType.ALL)
	@SortNatural
	private SortedSet<DeviceAssignment> assignments = new TreeSet<DeviceAssignment>();
	
	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	@PrimaryKeyJoinColumn
	private PersonCredential credential;

	public Long getId() {
		return id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getTermination() {
		return termination;
	}

	public void setTermination(Date termination) {
		this.termination = termination;
	}

	public SortedSet<DeviceAssignment> getAssignments() {
		return assignments;
	}

	public void setAssignments(SortedSet<DeviceAssignment> assignments) {
		this.assignments = assignments;
	}

	public PersonCredential getCredential() {
		return credential;
	}

	public void setCredential(PersonCredential credential) {
		this.credential = credential;
	}

	@Override
	public String toString() {
		return new StringBuilder("Person[id=").append(id).append("]").toString();
	}

	@Override
	public int compareTo(Person o) {
		return COMPARATOR.compare(this, o);
	}
}
