/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.domain;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortNatural;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * NOTE: this class has a natural ordering that is inconsistent with equals.
 */
@Entity
@Table(name="device")
public class Device implements Comparable<Device> {
	protected static final Comparator<Device> COMPARATOR = Comparator.comparing(Device::getRelease, Comparator.nullsLast(Date::compareTo).reversed())
			.thenComparing(Device::getTypeId)
			.thenComparing(Device::getOwned, Comparator.reverseOrder())
			.thenComparing(Device::getAcquisition, Comparator.nullsFirst(Date::compareTo).reversed())
			.thenComparing(Device::getName, String.CASE_INSENSITIVE_ORDER)
			.thenComparing(Device::getId);

	@Id
	@Column(name="device_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(length=100)
	@NotNull
	private String name;

	@Column(length=10000)
	private String description;

	@Column(length=100, unique=true)
	@NotNull
	private String serialNumber;

	/**
	 * True if organization owns device, false if renting or simply accessing.
	 */
	@Column
	private Boolean owned;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(iso=ISO.DATE)
	@NotNull
	private Date acquisition;
	
	//used "RELEASE_DATE" b/c "RELEASE" is a reserved word
	@Column(name="release_date")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(iso=ISO.DATE)
	private Date release;

	@Column(name="type_id")
	private short typeId;

	@OneToMany(mappedBy="device", cascade=CascadeType.ALL)
	@SortNatural
	private SortedSet<DeviceAudit> audits = new TreeSet<DeviceAudit>();

	@OneToMany(mappedBy="device", cascade=CascadeType.ALL)
	@SortNatural
	private SortedSet<DeviceAssignment> assignments = new TreeSet<DeviceAssignment>();

	//TODO: make
	//TODO: model

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Boolean getOwned() {
		return owned;
	}

	public void setOwned(Boolean owned) {
		this.owned = owned;
	}

	public Date getAcquisition() {
		return acquisition;
	}

	public void setAcquisition(Date acquisition) {
		this.acquisition = acquisition;
	}

	public Date getRelease() {
		return release;
	}

	public void setRelease(Date release) {
		this.release = release;
	}

	public short getTypeId() {
		return typeId;
	}

	public void setTypeId(short typeId) {
		this.typeId = typeId;
	}

	public Type getType() {
		return Type.get(typeId);
	}

	public SortedSet<DeviceAudit> getAudits() {
		return audits;
	}

	public void setAudits(SortedSet<DeviceAudit> audits) {
		this.audits = audits;
	}

	public SortedSet<DeviceAssignment> getAssignments() {
		return assignments;
	}

	public void setAssignments(SortedSet<DeviceAssignment> assignments) {
		this.assignments = assignments;
	}

	@Override
	public int compareTo(Device o) {
		return COMPARATOR.compare(this, o);
	}

	public enum Type {
		COMPUTER(1), SERVER(2), DISPLAY(3), NETWORKING(4), STORAGE(5), MISCELLANEOUS(6);

		private static final Map<Short, Type> byId = new HashMap<>();

		static {
			for (Type t: Type.values()) {
				byId.put(t.id, t);
			}
		}

		public static Type get(Short id) {
			return byId.get(id);
		}

		private final short id;

		Type(int id) {
			this.id = (short)id;
		}

		public short getId() {
			return id;
		}
	}
}
