/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.domain;

import java.util.Comparator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * NOTE: this class has a natural ordering that is inconsistent with equals.
 */
@Entity
//TODO: add unique constraint on (policy_id, email_address)
@Table(name="policy_request")
public class PolicyRequest implements Comparable<PolicyRequest> {
	protected static final Comparator<PolicyRequest> COMPARATOR = Comparator.comparing(PolicyRequest::getPolicyResponse, Comparator.nullsLast(PolicyResponse.COMPARATOR).reversed())
			.thenComparing(PolicyRequest::getEmailAddress, Comparator.nullsLast(String.CASE_INSENSITIVE_ORDER))
			.thenComparing(PolicyRequest::getId, Comparator.nullsFirst(Long::compareTo));

	@Id
	@Column(name="policy_request_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="policy_id")
	@NotNull
	private Policy policy;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	public PolicyResponse policyResponse;

	@Column(length=50, unique=true)
	@NotNull
	//TODO: specify fixed-width column?  case-sensitive collation?
	private String code;

	@Column(length=255)
	@NotNull
	private String emailAddress;
	
	@SuppressWarnings("unused")
	private PolicyRequest() { /* no-arg constructor for JPA */ }
	
	public PolicyRequest(Policy p, String addr, String code) {
		this.policy = p;
		this.emailAddress = addr;
		this.code = code;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Policy getPolicy() {
		return policy;
	}

	protected void setPolicy(Policy policy) {
		this.policy = policy;
	}

	public PolicyResponse getPolicyResponse() {
		return policyResponse;
	}

	public void setPolicyResponse(PolicyResponse policyResponse) {
		this.policyResponse = policyResponse;
	}

	public String getCode() {
		return code;
	}

	protected void setCode(String code) {
		this.code = code;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	protected void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public int compareTo(PolicyRequest o) {
		return COMPARATOR.compare(this, o);
	}

	@Override
	public String toString() {
		return new StringBuilder("PolicyRequest[id=").append(id).append(",emailAddress=").append(emailAddress).append(",code=").append(code).append("]").toString();
	}
}
