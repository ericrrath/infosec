/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Domain classes mapped with JPA.  Notes:
 * <ul>
 *     <li><code>Comparable</code> implementations should use
 *         <code>java.util.Comparator.comparing()</code></li>
 *     <li>Should I continue to use private no-arg constructor and
 *         final-member-assignment in public constructor?  Or just stick
 *         to no-arg constructors?
 *     <li>Should <code>Comparator</code> logic handle null ids, or expect non-null values?
 * </ul>
 */
package name.rath.eric.infosec.domain;
