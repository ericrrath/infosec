/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.domain;

import java.util.Comparator;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortNatural;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * NOTE: this class has a natural ordering that is inconsistent with equals.
 */
@Entity
@Table(name="policy")
public class Policy implements Comparable<Policy> {
	protected static final Comparator<Policy> COMPARATOR = Comparator.comparing(Policy::getName, String.CASE_INSENSITIVE_ORDER)
			.thenComparing(Policy::getId);

	@Id
	@Column(name="policy_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(length=100)
	@NotNull
	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(iso=ISO.DATE)
	private Date effective;
	
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date modified;

	//some policies require per-person response; others (e.g. secure workstation baseline) are used for audits
	@Column
	private Boolean requiresResponse;
	
	@Column(length=2000)
	@Lob
	@NotNull
	private String body;
	
	@OneToMany(mappedBy="policy", cascade=CascadeType.ALL)
	@SortNatural
	private SortedSet<PolicyRequest> requests = new TreeSet<PolicyRequest>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getEffective() {
		return effective;
	}

	public void setEffective(Date effective) {
		this.effective = effective;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Boolean getRequiresResponse() {
		return requiresResponse;
	}

	public void setRequiresResponse(Boolean requiresResponse) {
		this.requiresResponse = requiresResponse;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public SortedSet<PolicyRequest> getRequests() {
		return requests;
	}

	public void setRequests(SortedSet<PolicyRequest> requests) {
		this.requests = requests;
	}

	@Override
	public int compareTo(Policy o) {
		return COMPARATOR.compare(this, o);
	}

	public String getCacheKey() {
		//TODO: must be better behavior when id or mod is null
		if (id == null || modified == null) {
			return super.toString();
		}
		return String.format("Policy[id=%d,modified=%d]", id, modified.getTime());
	}
}
