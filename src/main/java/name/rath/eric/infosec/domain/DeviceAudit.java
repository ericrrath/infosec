/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.domain;

import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import name.rath.eric.infosec.domain.valid.Web;

/**
 * NOTE: this class has a natural ordering that is inconsistent with equals.
 */
@Entity
@Table(name="device_audit")
public class DeviceAudit implements Comparable<DeviceAudit> {
	protected static final Comparator<DeviceAudit> COMPARATOR = Comparator.comparing(DeviceAudit::getAuditDate, Comparator.nullsLast(Date::compareTo).reversed())
			.thenComparing(DeviceAudit::getId, Comparator.nullsFirst(Long::compareTo));

	@Id
	@Column(name="device_audit_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="device_id")
	@NotNull
	private Device device;
	
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(groups=Web.class)
	@Past(groups=Web.class)
	@DateTimeFormat(iso=ISO.DATE)
	private Date auditDate;

	@Column(length=10000)
	@NotNull(groups=Web.class)
	private String notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public int compareTo(DeviceAudit o) {
		return COMPARATOR.compare(this, o);
	}

	//TODO: is there a relevant policy
}
