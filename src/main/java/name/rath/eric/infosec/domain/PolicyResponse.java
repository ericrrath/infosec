/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec.domain;

import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
@Table(name="policy_response")
public class PolicyResponse {
	protected static final Comparator<PolicyResponse> COMPARATOR = Comparator.comparing(PolicyResponse::getAccept)
			.thenComparing(PolicyResponse::getId);

    private Long id;

	private Date responseDate;

	private String ipAddress;
	
	private Boolean accept;
	
	@Id
	@Column(name="policy_request_id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	@DateTimeFormat(iso=ISO.DATE_TIME)
	public Date getResponseDate() {
		return responseDate;
	}

	@Column(length=50)
	@NotNull
	public String getIpAddress() {
		return ipAddress;
	}

	@Column
	@NotNull
	public Boolean getAccept() {
		return accept;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public void setAccept(Boolean accept) {
		this.accept = accept;
	}
}
