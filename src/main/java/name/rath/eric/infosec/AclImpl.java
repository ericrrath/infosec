/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.acls.domain.CumulativePermission;
import org.springframework.security.acls.model.AccessControlEntry;
import org.springframework.security.acls.model.Acl;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;
import org.springframework.security.acls.model.UnloadedSidException;

/**
 * AclImpl defines an access control list for a given object.
 */
public class AclImpl implements Acl {
	private static final long serialVersionUID = -2647455220050856609L;
	
	private final List<AccessControlEntry> entries = new ArrayList<AccessControlEntry>();
	private final ObjectIdentity identity;
	private final Sid owner;
	
	public AclImpl(ObjectIdentity identity, Sid owner) {
		this.identity = identity;
		this.owner = owner;
	}
	
	@Override
	public List<AccessControlEntry> getEntries() {
		return entries;
	}

	@Override
	public ObjectIdentity getObjectIdentity() {
		return identity;
	}

	@Override
	public Sid getOwner() {
		return owner;
	}

	@Override
	public Acl getParentAcl() {
		return null;
	}

	@Override
	public boolean isEntriesInheriting() {
		return false;
	}

	/**
	 * @param assertedPermissions list of all permissions to be asserted
	 * @param sids ignored
	 * @param administrativeMode ignored
	 * @return true if the contained entries collectively grant all the asserted permissions.
	 */
	@Override
	public boolean isGranted(List<Permission> assertedPermissions, List<Sid> sids, boolean administrativeMode)
			throws NotFoundException, UnloadedSidException {
		CumulativePermission asserted = new CumulativePermission();
		assertedPermissions.stream().forEach(p -> asserted.set(p));
		CumulativePermission granted = new CumulativePermission();
		entries.stream().forEach(ace -> granted.set(ace.getPermission()));
		return (granted.getMask() & asserted.getMask()) == asserted.getMask();
	}

	/**
	 * @param sids ignored
	 * @return always returns true
	 */
	@Override
	public boolean isSidLoaded(List<Sid> sids) {
		return true;
	}
}
