/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric.infosec;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import name.rath.eric.infosec.domain.Device;
import name.rath.eric.infosec.domain.DeviceAssignment;
import name.rath.eric.infosec.domain.Person;
import name.rath.eric.infosec.domain.PersonCredential;
import name.rath.eric.infosec.domain.Policy;
import name.rath.eric.infosec.domain.PolicyRequest;

/**
 * Dao performs all persistence tasks (Create, Read, Update, Delete) using JPA 2.
 */
//TODO: use criteria queries?
@org.springframework.stereotype.Repository
public class Repository {
	@PersistenceContext
	EntityManager entityManager;

	/**
	 * @return policies sorted naturally (i.e. using Policy.compareTo(Policy))
	 */
	@SuppressWarnings("unchecked")
	public List<Policy> getPolicies() {
		List<Policy> policies = entityManager.createQuery("FROM Policy").getResultList();
		Collections.sort(policies);
		return policies;
	}

	@SuppressWarnings("unchecked")
	/**
	 * @param type optionally restrict to this type
	 * @return list of types, restricted by type if provided
	 */
	public List<Device> getDevices(Device.Type type) {
		Query q = null;
		if (type == null) {
			q = entityManager.createQuery("FROM Device");
		} else {
			q = entityManager.createQuery("FROM Device WHERE typeId = :typeId");
			q.setParameter("typeId", type.getId());
		}
		List<Device> devices = q.getResultList();
		Collections.sort(devices);
		return devices;
	}

	/**
	 * @return list of distinct device name values orderer alphabetically
	 */
	@SuppressWarnings("unchecked")
	public List<String> getDeviceNames() {
		return entityManager.createQuery("SELECT DISTINCT d.name FROM Device d ORDER BY d.name").getResultList();
	}

	/**
	 * @param <T> type to return
	 * @param type retrieve
	 * @param id of instance type to retrieve
	 * @return instance of type with given id
	 * @throws EntityNotFoundException if object not found
	 */
	public <T extends Object> T get(Class<T> type, Long id) {
		T obj = type.cast(entityManager.find(type, id));
		if (obj == null) {
			throw new EntityNotFoundException(type.getName() + "[id=" + id + "]");
		}
		return obj;
	}

	public Object save(Object obj) {
		return entityManager.merge(obj);
	}

	public void add(Object obj) {
		entityManager.persist(obj);
	}

	public void delete(Object obj) {
		entityManager.remove(obj);
	}

	/**
	 * @param code 50-character alphanumeric string, uniquely identifies policy request instance
	 * @return request with given code, or null if no response uses given code
	 */
	public PolicyRequest getPolicyRequest(String code) {
		Query query = entityManager.createQuery("FROM PolicyRequest WHERE code=:code");
		query.setParameter("code", code);
		List<?> results = query.getResultList();
		return (PolicyRequest) (results.isEmpty() ? null : results.get(0));
	}

	@SuppressWarnings("unchecked")
	public List<Person> getPersons(Boolean terminated) {
		Query q = null;
		if (terminated == null) {
			//don't restrict on termination
			q = entityManager.createQuery("FROM Person");
		} else if (terminated) {
			//restrict to terminated (i.e. non-null termination date) 
			q = entityManager.createQuery("FROM Person WHERE termination IS NOT NULL");
		} else {
			//restrict to unterminated (i.e. null termination date)
			q = entityManager.createQuery("FROM Person WHERE termination IS NULL");
		}
		List<Person> persons = q.getResultList();
		Collections.sort(persons);
		return persons;
	}

	/**
	 * @param username 20-character alphanumeric string, uniquely identifies person credential instance
	 * @return Person with given username; throws NoResultException if no Person found for username
	 */
	public PersonCredential getPersonCredential(String username) {
		Query query = entityManager.createQuery("FROM PersonCredential WHERE userName=:username");
		query.setParameter("username", username);
		return (PersonCredential) query.getSingleResult();
	}

	public long getPersonCount() {
		TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(p) FROM Person p", Long.class);
		return query.getSingleResult().longValue();
	}

	/**
	 * @return map of device id to most recent audit date for that device.
	 * 		devices without audits or already released will be excluded
	 */
	public Map<Long, Date> getMostRecentAuditDates() {
		Query query = entityManager.createQuery("SELECT d.id, MAX(a.auditDate) FROM DeviceAudit a JOIN a.device d WHERE d.release IS NULL GROUP BY d.id");
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();
		Map<Long, Date> maxDates = new HashMap<>();
		for (Object[] row: results) {
			maxDates.put((Long)row[0], (Date)row[1]);
		}
		return maxDates;
	}

	/**
	 * @return device assignments not yet released
	 */
	public List<DeviceAssignment> getCurrentDeviceAssignments() {
		TypedQuery<DeviceAssignment> query = entityManager.createQuery("FROM DeviceAssignment da WHERE da.released IS NULL", DeviceAssignment.class);
		return query.getResultList();
	}
}
