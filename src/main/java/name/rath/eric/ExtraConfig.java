/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import name.rath.eric.infosec.Repository;

/**
 * ExtraConfig defines a couple beans to avoid circular dependency problems on startup.
 */
/*
 * TODO: @PropertySource can be removed after upgrading to Spring Boot 1.4
 * see http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#production-ready-application-info-git
 */
@Configuration
@PropertySource("classpath:git.properties")
public class ExtraConfig {

	@Bean
	public Repository repository() {
		return new Repository();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
