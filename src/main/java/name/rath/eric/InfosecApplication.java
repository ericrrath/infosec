/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric;

import org.apache.catalina.connector.Connector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.acls.AclPermissionEvaluator;
import org.springframework.util.StringUtils;

import name.rath.eric.infosec.AclServiceImpl;

@SpringBootApplication
@ComponentScan({"org.owasp.appsensor", "name.rath.eric"})
public class InfosecApplication extends SpringBootServletInitializer {
	private static final Logger LOGGER = LoggerFactory.getLogger(InfosecApplication.class);
	private Integer ajpPort;

	public static void main(String[] args) {
		SpringApplication.run(InfosecApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(InfosecApplication.class);
	}

	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
		if (ajpPort != null) {
			LOGGER.info("adding AJP connector on port {}", ajpPort);
			Connector ajpConnector = new Connector("AJP/1.3");
			ajpConnector.setProtocol("AJP/1.3");
			ajpConnector.setPort(ajpPort.intValue());
			ajpConnector.setSecure(false);
			ajpConnector.setAllowTrace(false);
			ajpConnector.setScheme("http");
			tomcat.addAdditionalTomcatConnectors(ajpConnector);
		} else {
			LOGGER.debug("no AJP port specified in server.ajp.port; not adding AJP connector");
		}
		return tomcat;
	}

	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() {
		return (container -> {
			ErrorPage errorPage = new ErrorPage("/WEB-INF/views/error.jsp");
			container.addErrorPages(errorPage);
		});
	}

	/**
	 * The PermissionEvaluator instance is created here because it cannot be created
	 * in the security config.  See http://stackoverflow.com/questions/23638462/how-do-i-add-method-based-security-to-a-spring-boot-project
	 */
	@Bean
	@Autowired
	public PermissionEvaluator permissionEvaluator(AclServiceImpl service) {
		return new AclPermissionEvaluator(service);
	}

	@Value("${server.ajp.port}")
	public void setAjpPort(String ajpPort) {
		try {
			this.ajpPort = StringUtils.hasText(ajpPort) ? Integer.parseInt(ajpPort) : null;
		} catch (NumberFormatException nfe) {
			this.ajpPort = null;
			LOGGER.warn("invalid server.ajp.port value: {}", ajpPort);
		}
	}

}
