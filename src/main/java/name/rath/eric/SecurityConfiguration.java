/**
 * This file is part of Infosec.
 *
 * Infosec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Infosec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Infosec.  If not, see <http://www.gnu.org/licenses/>.
 */
package name.rath.eric;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;

import name.rath.eric.infosec.AuthenticationService;
import name.rath.eric.infosec.CrowdRestAuthenticationProvider;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityConfiguration.class);

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.formLogin().loginProcessingUrl("/public/authenticate").loginPage("/").failureUrl("/?error=t")
				.defaultSuccessUrl("/").usernameParameter("username").passwordParameter("password");
		httpSecurity.logout().logoutUrl("/signout").permitAll();
		httpSecurity.authorizeRequests().antMatchers("/public/**").permitAll();
		httpSecurity.authorizeRequests().antMatchers("/").permitAll();
		httpSecurity.authorizeRequests().antMatchers("/changePassword").permitAll();
		httpSecurity.authorizeRequests().antMatchers("/assets/**").permitAll();
		httpSecurity.authorizeRequests().antMatchers("/**").authenticated();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth, AuthenticationService authService,
			PasswordEncoder passwordEncoder, @Value("${security.crowd.api-url}") String crowdApiUrl,
			@Value("${security.crowd.username}") String crowdUsername,
			@Value("${security.crowd.password}") String crowdPassword) throws Exception {
		if (StringUtils.hasText(crowdApiUrl) && StringUtils.hasText(crowdUsername) && StringUtils.hasText(crowdPassword)) {
			LOGGER.info("adding Crowd authentication provider at {}", crowdApiUrl);
			auth.authenticationProvider(new CrowdRestAuthenticationProvider(crowdApiUrl, crowdUsername, crowdPassword));
		} else {
			LOGGER.debug("insufficient info found for Crowd authentication provider; not adding");
		}
		auth.userDetailsService(authService).passwordEncoder(passwordEncoder);
	}
}