<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag display-name="person" description="Renders name of person, with id title" %>
<%@ attribute name="value" required="true" type="name.rath.eric.infosec.domain.Person" %>
<a href="<c:url value="/persons/${value.id}" />" title="id=${value.id}"><c:out value="${value.firstName} ${value.lastName}" /></a>