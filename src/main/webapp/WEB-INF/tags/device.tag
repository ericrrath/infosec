<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ tag display-name="device" description="Renders name of person, with id title" %>
<%@ attribute name="value" required="true" type="name.rath.eric.infosec.domain.Device" %>
<a href="<c:url value="/devices/${value.id}" />" title="<spring:message code="device.serialNumber" />=<c:out value="${value.serialNumber}" />"><c:out value="${value.name}" /></a>