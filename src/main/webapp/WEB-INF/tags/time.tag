<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ tag display-name="device" description="Renders time element" %>
<%@ attribute name="value" required="true" type="java.util.Date" %>
<c:if test="${not empty value}">
<time datetime="<fmt:formatDate type='date' pattern='yyyy-MM-dd' value='${value}' />">
	<fmt:formatDate type="date" pattern="dd MMM YYYY" value="${value}" />
</time>
</c:if>