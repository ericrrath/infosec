<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ tag display-name="field" description="Renders tag body enclosed in bootstrap field markup" %>
<%@ attribute name="path" required="true" type="java.lang.String" %>
<spring:bind path="${path}">
<div class="form-group ${status.error ? 'has-error' : ''}">
	<jsp:doBody />
	<form:errors path="${path}" cssClass="text-danger" />
</div>
</spring:bind>