<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1>
	<a href="<c:url value="/persons" />"><spring:message code="persons" /></a>:
	<c:out value="${person.firstName} ${person.lastName}" />
	<c:if test="${not empty person.termination}">
		<span class="label label-default">Terminated</span>
	</c:if>
</h1>
<div class="btn-group">
	<sec:accesscontrollist domainObject="${person}" hasPermission="WRITE">
	<a href="<c:url value="/persons/${person.id}/edit" />" class="btn btn-default"><spring:message code="generic.command.edit" /></a>
	</sec:accesscontrollist>
	<sec:accesscontrollist domainObject="${person}" hasPermission="DELETE">
	<a href="<c:url value="/persons/${person.id}/delete" />" class="btn btn-default"><spring:message code="generic.command.delete" /></a>
	</sec:accesscontrollist>
</div>
<dl>
	<dt><spring:message code="generic.label.created" /></dt>
	<dd><is:time value="${person.created}" /></dd>
	<dt><spring:message code="person.termination" /></dt>
	<dd><is:time value="${person.termination}" /></dd>
</dl>

<table class="table table-striped">
<caption>Device assignments</caption>
<thead>
<tr>
	<th><spring:message code="device.serialNumber" /></th>
	<th><spring:message code="device.name" /></th>
	<th>Assigned</th>
	<th>Released</th>
</tr>
</thead>
<tbody>
<tbody>
<c:choose><c:when test="${not empty person.assignments}">
<c:forEach items="${person.assignments}" var="assignment">
	<tr>
		<td><kbd><c:out value="${assignment.device.serialNumber}" /></kbd></td>
		<td><is:device value="${assignment.device}" /></td>
		<td><is:time value="${assignment.assigned}" /></td>
		<td><is:time value="${assignment.released}" /></td>
	</tr>
</c:forEach>
</c:when><c:otherwise>
	<tr><td colspan="4"><spring:message code="generic.label.none" /></td></tr>
</c:otherwise></c:choose>
</table>
