<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1>
	<a href="<c:url value="/persons" />"><spring:message code="persons" /></a>:
	<c:out value="${person.firstName} ${person.lastName}" />
</h1>
<form:form modelAttribute="device" method="post">
	<p class="form-control-static">Are you sure you want to delete the person "<c:out value="${person.firstName} ${person.lastName}" />"?</p>
	<div class="form-group">
		<button type="submit" class="btn btn-primary"><spring:message code="generic.command.delete" /></button>
		<button type="submit" name="_cancel" value="_cancel" class="btn"><spring:message code="generic.command.cancel" /></button>
	</div>
</form:form>
