<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1><spring:message code="persons" /></h1>
<p>
People do things.
</p>
<ul class="nav nav-tabs">
	<li role="presentation" <c:if test="${empty param.terminated}">class="active"</c:if>><a href="<c:url value="/persons" />">All</a></li>
	<li role="presentation" <c:if test="${not empty param.terminated and not param.terminated}">class="active"</c:if>><a href="<c:url value="/persons?terminated=false" />">Active</a></li>
	<li role="presentation" <c:if test="${not empty param.terminated and param.terminated}">class="active"</c:if>><a href="<c:url value="/persons?terminated=true" />">Terminated</a></li>
</ul>
<table class="table table-striped">
<thead>
<tr>
	<th>First name</th>
	<th>Last name</th>
	<th><spring:message code="generic.label.created" /></th>
	<th><spring:message code="person.termination" /></th>
</tr>
</thead>
<tbody>
<c:forEach items="${persons}" var="p">
	<tr <c:if test="${not empty p.termination}">class="text-muted"</c:if>>
		<td><a href="<c:url value="/persons/${p.id}" />"><c:out value="${p.lastName}" /></a></td>
		<td><c:out value="${p.firstName}" /></td>
		<td><is:time value="${p.created}" /></td>
		<td><is:time value="${p.termination}" /></td>
	</tr>
</c:forEach>
	<tr>
		<td colspan="4">
			<a href="<c:url value="/persons/new" />" class="btn btn-primary btn-small active"><spring:message code="generic.command.add" /></a>
		</td>
	</tr>
</tbody>
</table>
