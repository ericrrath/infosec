<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="i" uri="http://eric.rath.name/infosec" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1>
	<a href="<c:url value="/persons" />"><spring:message code="persons" /></a>:
	<c:choose>
		<c:when test="${not empty person.id}"><is:person value="${person}" /></c:when>
		<c:otherwise><spring:message code="generic.command.add" /></c:otherwise>
	</c:choose>
</h1>
<form:form modelAttribute="person" method="post">
	<is:field path="firstName">
		<label class="control-label" for="firstName">
			<spring:message code="person.first-name" />
		</label>
		<form:input type="text" path="firstName" maxlength="50" class="form-control" />
	</is:field>
	<is:field path="lastName">
		<label class="control-label" for="lastName">
			<spring:message code="person.last-name" />
		</label>
		<form:input type="text" path="lastName" maxlength="50" class="form-control" />
	</is:field>
	<is:field path="termination">
		<label class="control-label" for="termination">
			<spring:message code="person.termination" />
		</label>
		<form:input type="text" path="termination" class="form-control" data-type="date" />
	</is:field>
	<div class="form-group">
		<button type="submit" class="btn btn-primary"><spring:message code="generic.command.save" /></button>
		<button type="submit" name="_cancel" value="_cancel" class="btn"><spring:message code="generic.command.cancel" /></button>
	</div>
</form:form>