<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<form class="form-inline" role="search" method="get" action="<c:url value="/search" />">
	<div class="form-group">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search" name="term" value="<c:out value="${param.term}" />">
		</div>
		<button type="submit" class="btn btn-primary">Search</button>
	</div>
</form>
<c:if test="${not empty searchResponse}">
<hr />
<div class="panel ${searchResponse.hits.totalHits gt 0 ? 'panel-success' : 'panel-default'}">
	<div class="panel-heading">
		<c:out value="${searchResponse.hits.totalHits} result(s) in ${searchResponse.tookInMillis}ms"/>
	</div>
	<c:if test="${not empty searchResponse.hits.hits}">
	<ol class="list-group">
	<c:forEach items="${searchResponse.hits.hits}" var="hit">
		<li class="list-group-item">
		<c:choose>
			<c:when test="${hit.type eq 'device'}">
			<a href="<c:url value="/devices/${hit.id}" />"><c:out value="${hit.field('name').value()}" /> <kbd><c:out value="${hit.field('serial-number').value()}" /></kbd></a>
			</c:when>
			<c:when test="${hit.type eq 'audit'}">
			<a href="<c:url value="/devices/${hit.field('device-id').value()}/audits/${hit.id}" />">Audit</a>
			</c:when>
		</c:choose>
		<c:if test="${not empty hit.highlightFields}">
			<c:forEach items="${hit.highlightFields}" var="h">
				<c:forEach items="${h.value.fragments}" var="f">
				<p>"<c:out value="${f}" escapeXml="false" />"</p>
				</c:forEach>
			</c:forEach>
		</c:if>
		</li>
	</c:forEach>
	</ol>
	</c:if>
</div>
</c:if>
