<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page isErrorPage="true" %>
<spring:eval var="appVersion" expression="@environment.getProperty('info.build.version')" />
<!DOCTYPE html>
<html lang="en-us">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<c:url value="/assets/img/favicon.ico" />" />
	<link href="<c:url value="/assets/styles/all.css?v=${appVersion}" />" rel="stylesheet">
	<title><spring:message code="error.${pageContext.errorData.statusCode}.title" text="Error" /></title>
</head>
<body>
	<div class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href='<c:url value="/" />'><spring:message code="application.title" text="Infosec" /></a>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<div class="container">
		<div class="alert alert-danger">
			<span class="label label-danger"><c:out value="${pageContext.errorData.statusCode}" /></span>
			<spring:message code="error.${pageContext.errorData.statusCode}.message" text="An error has occured" />
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<p class="text-muted">v<c:out value="${appVersion}" /></p>
		</div>
	</footer>
	<script src="<c:url value="/assets/scripts/all-min.js?v=${appVersion}" />"></script>
</body>
</html>