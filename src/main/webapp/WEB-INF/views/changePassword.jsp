<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1>
	<spring:message code="person.password.change" />
</h1>
<form:form modelAttribute="passwordChange" method="post">
	<is:field path="current">
		<label class="control-label" for="current">
			<spring:message code="person.password.current" />
		</label>
		<form:input type="password" path="current" maxlength="50" class="form-control" />
	</is:field>
	<is:field path="newPassword">
		<label class="control-label" for="new">
			<spring:message code="person.password.new" />
		</label>
		<form:input type="password" path="newPassword" maxlength="50" class="form-control" />
	</is:field>
	<is:field path="newPasswordConfirm">
		<label class="control-label" for="newConfirm">
			<spring:message code="person.password.new-confirm" />
		</label>
		<form:input type="password" path="newPasswordConfirm" maxlength="50" class="form-control" />
	</is:field>
	<div class="form-group">
		<button type="submit" class="btn btn-primary"><spring:message code="generic.command.save" /></button>
		<button type="submit" name="_cancel" value="_cancel" class="btn"><spring:message code="generic.command.cancel" /></button>
	</div>
</form:form>