<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="isAnonymous()">
<form class="form-inline" role="form" method="post" action="<c:url value="/public/authenticate" />">
	<div class="form-group <c:if test='${not empty param.error}'>has-error</c:if>">
		<label class="sr-only" for="j_username">username</label>
		<input type="text" class="form-control" name="username" id="username" placeholder="username" autofocus>
	</div>
	<div class="form-group <c:if test='${not empty param.error}'>has-error</c:if>">
		<label class="sr-only" for="password">password</label>
		<input type="password" class="form-control" name="password" id="password" placeholder="password">
	</div>
	<button type="submit" class="btn btn-primary">Sign in</button>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<br />
</sec:authorize>

<div class="jumbotron">
	<p>
		Infosec intends to help build and maintain good habits that improve your organization's security posture.
	</p>
</div>
