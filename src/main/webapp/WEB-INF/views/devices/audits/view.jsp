<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="i" uri="http://eric.rath.name/infosec" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1>
	<a href="<c:url value="/devices" />"><spring:message code="devices" /></a>:
	<is:device value="${audit.device}" />:
	<spring:message code="device.audit" />
</h1>
<div class="btn-group">
	<sec:accesscontrollist domainObject="${audit}" hasPermission="WRITE">
	<a href="<c:url value="/devices/${audit.device.id}/audits/${audit.id }/edit" />" class="btn btn-default"><spring:message code="generic.command.edit" /></a>
	</sec:accesscontrollist>
	<sec:accesscontrollist domainObject="${audit}" hasPermission="DELETE">
	<a href="<c:url value="/devices/${audit.device.id}/audits/${audit.id }/delete" />" class="btn btn-default"><spring:message code="generic.command.delete" /></a>
	</sec:accesscontrollist>
</div>
<dl>
	<dt><spring:message code="device.audit.date" /></dt>
	<dd><is:time value="${audit.auditDate}"/></dd>
</dl>
<c:if test="${not empty audit.notes}">
<div class="panel panel-default">
	<div class="panel-body">
		<i:markdown value="${audit.notes}" />
	</div>
</div>
</c:if>
