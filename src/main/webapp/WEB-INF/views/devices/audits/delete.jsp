<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1>
	<a href="<c:url value="/devices" />"><spring:message code="devices" /></a>:
	<is:device value="${deviceAudit.device}" />:
	<spring:message code="device.audit" />
</h1>
<form:form modelAttribute="deviceAudit" method="post">
	<p class="form-control-static">Are you sure you want to delete the device audit for <is:time value="${deviceAudit.auditDate}" />?</p>
	<div class="form-group">
		<button type="submit" class="btn btn-primary"><spring:message code="generic.command.delete" /></button>
		<button type="submit" name="_cancel" value="_cancel" class="btn"><spring:message code="generic.command.cancel" /></button>
	</div>
</form:form>
