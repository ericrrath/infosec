<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<h1>
	<a href="<c:url value="/devices" />"><spring:message code="devices" /></a>:
	<is:device value="${device}" />:
	<spring:message code="device.audit" />
</h1>
<form:form modelAttribute="deviceAudit" method="post">
	<is:field path="notes">
		<label class="control-label" for="notes">
			<spring:message code="device.audit.notes" />
		</label>
		<form:textarea path="notes" rows="5" cols="80" class="form-control"/>
	</is:field>
	<is:field path="auditDate">
		<label class="control-label" for="auditDate">
			Audit date
		</label>
		<form:input path="auditDate" class="form-control" data-type="date"/>
	</is:field>
	<div class="form-group">
		<button type="submit" class="btn btn-primary"><spring:message code="generic.command.save" /></button>
		<button type="submit" name="_cancel" value="_cancel" class="btn"><spring:message code="generic.command.cancel" /></button>
	</div>
</form:form>