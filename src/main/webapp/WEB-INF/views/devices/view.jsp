<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="i" uri="http://eric.rath.name/infosec" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1>
	<a href="<c:url value="/devices" />"><spring:message code="devices" /></a>:
	<c:out value="${device.name}" />
	<c:if test="${not empty device.release}">
		<span class="label label-default"><spring:message code="device.released" /></span>
	</c:if>
</h1>
<div class="btn-group">
	<sec:accesscontrollist domainObject="${device}" hasPermission="WRITE">
	<a href="<c:url value="/devices/${device.id}/edit" />" class="btn btn-default"><spring:message code="generic.command.edit" /></a>
	</sec:accesscontrollist>
	<sec:accesscontrollist domainObject="${device}" hasPermission="DELETE">
	<a href="<c:url value="/devices/${device.id}/delete" />" class="btn btn-default"><spring:message code="generic.command.delete" /></a>
	</sec:accesscontrollist>
</div>
<dl>
	<dt><spring:message code="device-type" /></dt>
	<dd><span class="label label-default"><spring:message code="device-type.${device.typeId}" /></span></dd>
	<dt><spring:message code="device.serialNumber" /></dt>
	<dd><kbd><c:out value="${device.serialNumber}" /></kbd></dd>
	<dt><spring:message code="device.owned" /></dt>
	<dd><c:choose><c:when test="${device.owned}"><span class="label label-primary">Owned</span></c:when><c:otherwise><span class="label label-default">Not owned</span></c:otherwise></c:choose></dd>
	<dt><spring:message code="device.acquisition" /></dt>
	<dd><is:time value="${device.acquisition}" /></dd>
	<c:if test="${not empty device.release}">
	<dt><spring:message code="device.release" /></dt>
	<dd><is:time value="${device.release}" /></dd>
	</c:if>
	<dt><spring:message code="device.age" /></dt>
	<dd><i:duration start="${device.acquisition}" end="${device.release}" /></dd>
</dl>
<c:if test="${not empty device.description}">
<div class="panel panel-default">
	<div class="panel-body">
		<i:markdown value="${device.description}" />
	</div>
</div>
</c:if>

<table class="table table-striped">
<caption>Audits</caption>
<thead>
<tr>
	<th>Date</th>
	<th>Notes</th>
</tr>
</thead>
<tbody>
<c:choose><c:when test="${not empty device.audits}">
<c:forEach items="${device.audits}" var="audit">
	<tr>
		<td>
			<a href="<c:url value="/devices/${device.id}/audits/${audit.id}" />">
				<is:time value="${audit.auditDate}" />
			</a>
		</td>
		<td><c:out value="${fn:substring(audit.notes, 0, 50)}" /></td>
	</tr>
</c:forEach>
</c:when><c:otherwise>
	<tr><td colspan="2"><spring:message code="generic.label.none" /></td></tr>
</c:otherwise></c:choose>
	<sec:accesscontrollist domainObject="${device}" hasPermission="CREATE">
	<tr>
		<td colspan="2">
			<a href="<c:url value="/devices/${device.id}/audits/new" />" class="btn btn-primary btn-small"><spring:message code="generic.command.add" /></a>
		</td>
	</tr>
	</sec:accesscontrollist>
</table>

<table class="table table-striped">
<caption>Assignments</caption>
<thead>
<tr>
	<th><spring:message code="person" /></th>
	<th>Assigned</th>
	<th>Released</th>
</tr>
</thead>
<c:set var="assignable" value="true" />
<tbody>
<c:choose><c:when test="${not empty device.assignments}">
<c:forEach items="${device.assignments}" var="assignment">
	<tr>
		<td><is:person value="${assignment.person}" /></td>
		<td><is:time value="${assignment.assigned}" /></td>
		<td>
			<c:choose><c:when test="${empty assignment.released}">
			<sec:accesscontrollist domainObject="${device}" hasPermission="CREATE">
				<c:url var="releaseUrl" value="/devices/${device.id}/release">
					<c:param name="assignmentId" value="${assignment.id}" />
				</c:url>
				<a href="${releaseUrl}" class="btn btn-default btn-sm active"><spring:message code="device.command.release" /></a>
			</sec:accesscontrollist>
			</c:when><c:otherwise>
				<is:time value="${assignment.released}" />
			</c:otherwise></c:choose>
		</td>
	</tr>
	<c:if test="${empty assignment.released}">
		<c:set var="assignable" value="false" />
	</c:if>
</c:forEach>
</c:when><c:otherwise>
	<tr><td colspan="3"><spring:message code="generic.label.none" /></td></tr>
</c:otherwise></c:choose>
<sec:accesscontrollist domainObject="${device}" hasPermission="CREATE">
<c:if test="${assignable}">
	<tr>
		<td colspan="3">
			<a href="<c:url value="/devices/${device.id}/assign" />" class="btn btn-primary active">
				<spring:message code="device.command.assign" />
			</a>
		</td>
	</tr>
</c:if>
</sec:accesscontrollist>
</tbody>
</table>
