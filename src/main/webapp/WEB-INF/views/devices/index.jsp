<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="i" uri="http://eric.rath.name/infosec" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1><spring:message code="devices" /></h1>
<a href="<c:url value="/devices/summary" />" >Date-limited summary</a>
<p>
Devices (e.g. desktop and laptop computers, tablets, mobile phones, servers) play a critical role
in information security.  Software and configurations that affect security can be changed by end-users, 
administrators, or even attackers in the case of an intrusion.  They can also be upgraded, and new 
software can be installed.  All of these changes affect the security of the information on the devices,
or that passes through the devices.  Devices should be audited periodically against an appropriate
policy or baseline.
</p>

<%-- scriptlet, but should be safe --%>
<c:set var="deviceTypes" value="<%= name.rath.eric.infosec.domain.Device.Type.values() %>" />
<ul class="nav nav-tabs">
	<li role="presentation" <c:if test="${empty param.typeId}">class="active"</c:if>><a href="<c:url value="/devices" />">All</a></li>
	<c:forEach items="${deviceTypes}" var="dt">
	<li role="presentation" <c:if test="${param.typeId eq dt.id}">class="active"</c:if>><a href="<c:url value="/devices?typeId=${dt.id}" />"><spring:message code="device-type.${dt.id}" /></a></li>
	</c:forEach>
</ul>
<table class="table table-striped">
<thead>
<tr>
	<c:if test="${empty param.typeId}"><th><spring:message code="device-type" /></th></c:if>
	<th><spring:message code="device.name" /></th>
	<th><spring:message code="device.serialNumber" /></th>
	<th><spring:message code="device.owned" /></th>
	<th><spring:message code="device.age" /></th>
	<th><spring:message code="device.assignee" /></th>
	<th><spring:message code="device.audit.date" /></th>
</tr>
</thead>
<tbody>
<%-- date instance used for calculating date diffs below --%>
<jsp:useBean id="now" class="java.util.Date" />
<c:forEach items="${devices}" var="d">
	<tr <c:if test="${not empty d.release}">class="text-muted"</c:if>>
		<c:if test="${empty param.typeId}"><td><span class="label label-default"><spring:message code="device-type.${d.typeId}" /></span></td></c:if>
		<td><a href="<c:url value="/devices/${d.id}" />"><c:out value="${d.name}" /></a></td>
		<td><kbd><c:out value="${d.serialNumber}" /></kbd></td>
		<td><c:choose><c:when test="${d.owned}"><span class="label label-primary">Owned</span></c:when><c:otherwise><span class="label label-default">Not owned</span></c:otherwise></c:choose></td>
		<td><i:duration start="${d.acquisition}" end="${d.release}" /></td>
		<td>
			<c:set var="assignee" value="${assignments[d.id]}" />
			<c:if test="${not empty assignee}">
				<is:person value="${assignee}" />
			</c:if>
		</td>
		<td>
			<c:set var="auditDate" value="${auditDates[d.id]}" />
			<c:choose>
				<c:when test="${not empty d.release}">
					<span class="label label-default"><spring:message code="device.released" /></span>
				</c:when>
				<c:when test="${not empty auditDate}">
					<c:set var="diff" value="${now.time - auditDate.time}" />
					<c:choose>
						<c:when test="${diff > 0 and diff < 1000 * 60 * 60 * 24 * 7}">
							<c:set var="cssClass" value="label-success" />
						</c:when>
						<c:when test="${diff > 1000 * 60 * 60 * 24 * 365}">
							<c:set var="cssClass" value="label-danger" />
						</c:when>
						<c:when test="${diff > 1000 * 60 * 60 * 24 * 335}">
							<c:set var="cssClass" value="label-warning" />
						</c:when>
						<c:otherwise>
							<c:set var="cssClass" value="label-default" />
						</c:otherwise>
					</c:choose>
					<span class="label ${cssClass}"><is:time value="${auditDate}" /></span>
				</c:when>
				<c:otherwise>
					<span class="label label-danger"><spring:message code="generic.label.none" /></span>
				</c:otherwise>
			</c:choose>
		</td>
	</tr>
</c:forEach>
	<tr>
		<td colspan="6">
			<a href="<c:url value="/devices/new" />" class="btn btn-primary btn-small"><spring:message code="generic.command.add" /></a>
		</td>
	</tr>
</tbody>
</table>
