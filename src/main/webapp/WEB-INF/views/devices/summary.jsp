<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1><spring:message code="devices" /> Summary</h1>

<form method="get" class="form-inline">
	<div class="form-group">
		<label for="start">Start</label>
		<input id="start" name="start" type="text" class="form-control" data-type="date" value="<fmt:formatDate type="date" pattern="yyyy-MM-dd" value="${start}" />" />
	</div>
	<div class="form-group">
		<label for="end">End</label>
		<input id="end" name="end" type="text" class="form-control" data-type="date" value="<fmt:formatDate type="date" pattern="yyyy-MM-dd" value="${end}" />" />
	</div>
	<div class="form-group">
		<select name="owned" class="form-control">
			<option value="" <c:if test="${empty owned}">selected="selected"</c:if>>All</option>
			<option value="true" <c:if test="${not empty owned and owned}">selected="selected"</c:if>>Owned</option>
			<option value="false" <c:if test="${not empty owned and not owned}">selected="selected"</c:if>>Not owned</option>
		</select>
	</div>
	<button type="submit" class="btn btn-primary btn-small">Update</button>
</form>
<table class="table table-striped">
<thead>
<tr>
	<th><spring:message code="device.name" /></th>
	<th><spring:message code="device.serialNumber" /></th>
	<th><spring:message code="device.owned" /></th>
	<th><spring:message code="device.acquisition" /></th>
	<th><spring:message code="device.release" /></th>
</tr>
</thead>
<tbody id="acquired">
<tr><th colspan="5">Acquired</th></tr>
<c:forEach items="${summary['acquired']}" var="d">
	<tr <c:if test="${not empty d.release}">class="text-muted"</c:if>>
		<td><a href="<c:url value="/devices/${d.id}" />"><c:out value="${d.name}" /></a></td>
		<td><kbd><c:out value="${d.serialNumber}" /></kbd></td>
		<td><c:choose><c:when test="${d.owned}"><span class="label label-primary">Owned</span></c:when><c:otherwise><span class="label label-default">Not owned</span></c:otherwise></c:choose></td>
		<td>
			<is:time value="${d.acquisition}" />
		</td>
		<td>
			<is:time value="${d.release}" />
		</td>
	</tr>
</c:forEach>
</tbody>

<tbody id="released">
<tr><th colspan="5">Released</th></tr>
<c:forEach items="${summary['released']}" var="d">
	<tr <c:if test="${not empty d.release}">class="text-muted"</c:if>>
		<td><a href="<c:url value="/devices/${d.id}" />"><c:out value="${d.name}" /></a></td>
		<td><kbd><c:out value="${d.serialNumber}" /></kbd></td>
		<td><c:choose><c:when test="${d.owned}"><span class="label label-primary">Owned</span></c:when><c:otherwise><span class="label label-default">Not owned</span></c:otherwise></c:choose></td>
		<td>
			<is:time value="${d.acquisition}" />
		</td>
		<td>
			<is:time value="${d.release}" />
		</td>
	</tr>
</c:forEach>
</tbody>

<tbody id="retained">
<tr><th colspan="5">Retained</th></tr>
<c:forEach items="${summary['retained']}" var="d">
	<tr <c:if test="${not empty d.release}">class="text-muted"</c:if>>
		<td><a href="<c:url value="/devices/${d.id}" />"><c:out value="${d.name}" /></a></td>
		<td><kbd><c:out value="${d.serialNumber}" /></kbd></td>
		<td><c:choose><c:when test="${d.owned}"><span class="label label-primary">Owned</span></c:when><c:otherwise><span class="label label-default">Not owned</span></c:otherwise></c:choose></td>
		<td>
			<is:time value="${d.acquisition}" />
		</td>
		<td>
			<is:time value="${d.release}" />
		</td>
	</tr>
</c:forEach>
</tbody>
</table>
