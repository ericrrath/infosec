<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<h1>
	<a href="<c:url value="/devices" />"><spring:message code="devices" /></a>:
	<is:device value="${device}" />
</h1>
<form:form modelAttribute="device" method="post">
	<is:field path="name">
		<label class="control-label" for="name">
			<spring:message code="device.name" />
		</label>
		<form:input type="text" path="name" list="devNames" maxlength="100" class="form-control"/>
		<datalist id="devNames">
			<c:forEach items="${names}" var="name">
			<option value="<c:out value="${name}" />">
			</c:forEach>
		</datalist>
	</is:field>
	<is:field path="description">
		<label class="control-label" for="desc">
			<spring:message code="generic.description" />
		</label>
		<form:textarea path="description" rows="5" cols="70" class="form-control"/>
	</is:field>
	<is:field path="typeId">
		<label class="control-label" for="typeId">
			<spring:message code="device-type" />
		</label>
		<form:select path="typeId" class="form-control">
			<form:option value="1"><spring:message code="device-type.1" /></form:option>
			<form:option value="2"><spring:message code="device-type.2" /></form:option>
			<form:option value="3"><spring:message code="device-type.3" /></form:option>
			<form:option value="4"><spring:message code="device-type.4" /></form:option>
			<form:option value="5"><spring:message code="device-type.5" /></form:option>
			<form:option value="6"><spring:message code="device-type.6" /></form:option>
		</form:select>
	</is:field>
	<%-- not using is:field because it doesn't yet gracefully handle checkboxes --%>
	<spring:bind path="owned">
	<div class="checkbox ${status.error ? 'has-error' : ''}">
		<label>
			<form:checkbox path="owned" />
			<spring:message code="device.owned" />
		</label>
		<form:errors path="owned" cssClass="text-danger" />
	</div>
	</spring:bind>
	<is:field path="serialNumber">
		<label class="control-label" for="serialNumber">
			<spring:message code="device.serialNumber" />
		</label>
		<form:input type="text" path="serialNumber" maxlength="100" class="form-control" />
	</is:field>
	<is:field path="acquisition">
		<label class="control-label" for="acquisition">
			<spring:message code="device.acquisition" />
		</label>
		<form:input type="text" path="acquisition" class="form-control" data-type="date" />
	</is:field>
	<c:if test="${not empty device.id}">
		<is:field path="release">
			<label class="control-label" for="release">
				<spring:message code="device.release" />
			</label>
			<form:input type="text" path="release" class="form-control" data-type="date" />
		</is:field>
	</c:if>
	<div class="form-group">
		<button type="submit" class="btn btn-primary"><spring:message code="generic.command.save" /></button>
		<button type="submit" name="_cancel" value="_cancel" class="btn"><spring:message code="generic.command.cancel" /></button>
	</div>
</form:form>