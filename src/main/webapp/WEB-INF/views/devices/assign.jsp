<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<h1>
	<a href="<c:url value="/devices" />"><spring:message code="devices" /></a>:
	<is:device value="${device}" />:
	Assign
</h1>
<form method="post">
	<div class="form-group ${status.error ? 'has-error' : ''}">
		<label class="control-label" for="notes">
			<spring:message code="person" />
		</label>
		<select name="personId">
		<c:forEach items="${persons}" var="p">
			<option value="${p.id}"><c:out value="${p.lastName}, ${p.firstName}" /></option>
		</c:forEach>
		</select>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-primary"><spring:message code="device.command.assign" /></button>
		<button type="submit" name="_cancel" value="_cancel" class="btn"><spring:message code="generic.command.cancel" /></button>
	</div>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>