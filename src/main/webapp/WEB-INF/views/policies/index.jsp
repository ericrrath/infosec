<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<h1>Policies</h1>
<p>
Policies are an effective way to define environment baselines, communicate behavioral expectations,
and communicate organizational responsibilities.  The factors that affect policies can change over
time; policies should be re-evaluated periodically, and updated as necessary.
</p>
<table class="table table-striped">
<thead>
<tr>
	<th>Name</th>
	<th>Effective</th>
	<th>Requires response</th>
</tr>
</thead>
<tbody>
<c:forEach items="${policies}" var="p">
	<tr>
		<td><a href="<c:url value="/policies/${p.id}" />"><c:out value="${p.name}" /></a></td>
		<td>
			<is:time value="${p.effective}" />
		</td>
		<td>
			<c:choose><c:when test="${p.requiresResponse}">
				<span class="label label-info">Required</span>				
			</c:when><c:otherwise>
				<span class="label">Not required</span>
			</c:otherwise></c:choose>
		</td>
	</tr>
</c:forEach>
	<tr>
		<td colspan="3">
			<a href="<c:url value="/policies/new" />" class="btn btn-primary btn-small"><spring:message code="generic.command.add" /></a>
		</td>
	</tr>
</tbody>
</table>
