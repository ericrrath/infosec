<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<h1>
	<a href="<c:url value="/policies" />">Policies</a>:
	<c:out value="${policy.name}" />
</h1>
<div class="btn-group">
	<sec:accesscontrollist domainObject="${policy}" hasPermission="WRITE">
	<a href="<c:url value="/policies/${policy.id}/edit" />" class="btn btn-default"><spring:message code="generic.command.edit" /></a>
	</sec:accesscontrollist>
	<a href="<c:url value="/policies/${policy.id}/sendRequest" />" class="btn btn-default">Send requests</a>
	<sec:accesscontrollist domainObject="${policy}" hasPermission="DELETE">
	<a href="<c:url value="/policies/${policy.id}/delete" />" class="btn btn-default"><spring:message code="generic.command.delete" /></a>
	</sec:accesscontrollist>
</div>
<table class="table table-striped">
<caption>Requests</caption>
<thead>
<tr>
	<th>Email</th>
	<th>Response</th>
	<th>Date</th>
	<th>IP Address</th>
</tr>
</thead>
<tbody>
<c:choose><c:when test="${not empty policy.requests}">
<c:forEach items="${policy.requests}" var="req">
	<tr>
		<td><c:out value="${req.emailAddress}" /></td>
		<td>
			<c:choose><c:when test="${not empty req.policyResponse}">
				<c:choose><c:when test="${req.policyResponse.accept}">
					<span class="label label-success">Accepted</span>				
				</c:when><c:otherwise>
					<span class="label label-important">Declined</span>
				</c:otherwise></c:choose>
			</c:when><c:otherwise>
				<span class="label">No response yet</span>
			</c:otherwise></c:choose>
		</td>
		<td>
			<c:if test="${not empty req.policyResponse}">
				<is:time value="${req.policyResponse.responseDate}" />
			</c:if>
		</td>
		<td>
			<c:if test="${not empty req.policyResponse}">
				<c:out value="${req.policyResponse.ipAddress}" />
			</c:if>
		</td>
	</tr>
</c:forEach>
</c:when><c:otherwise>
	<tr><td colspan="4"><spring:message code="generic.label.none" /></td></tr>
</c:otherwise></c:choose>
</tbody>
</table>
<div class="well">
<c:out value="${bodyMarkup}" escapeXml="false" />
</div>