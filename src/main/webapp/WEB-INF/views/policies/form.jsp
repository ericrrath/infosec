<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1>Policy</h1>
<form:form modelAttribute="policy" method="post">
	<is:field path="name">
		<label class="control-label" for="name">
			Name
		</label>
		<form:input type="text" path="name" maxlength="100" class="form-control" />
	</is:field>
	<is:field path="body">
		<label class="control-label" for="body">
			Body
		</label>
		<form:textarea path="body" rows="5" cols="80" class="form-control"/>
	</is:field>
	<div class="form-group">
		<button type="submit" class="btn btn-primary"><spring:message code="generic.command.save" /></button>
		<button type="submit" name="_cancel" value="_cancel" class="btn"><spring:message code="generic.command.cancel" /></button>
	</div>
</form:form>
