<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="is" tagdir="/WEB-INF/tags" %>

<c:choose><c:when test="${not empty policyRequest}">
<h1><spring:message code="policy.title" arguments="${policyRequest.policy.name}" argumentSeparator="|" /></h1>
<c:if test="${not empty policyRequest.policyResponse}">
	<span class="label ${policyRequest.policyResponse.accept ? 'label-success' : 'label-default'}">
	<spring:message code="policy.request.already-responded.${policyRequest.policyResponse.accept}" />
	<is:time value="${policyRequest.policyResponse.responseDate}" />
	</span>
</c:if>
<div>
<c:out value="${bodyMarkup}" escapeXml="false" />
</div>
<c:if test="${empty policyRequest.policyResponse}">
	<form method="post">
		<button type="submit" name="accept" value="true" class="btn btn-primary"><spring:message code="policy.request.accept" /></button>
		<button type="submit" name="accept" value="false" class="btn"><spring:message code="policy.request.decline" /></button>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</c:if>
</c:when><c:otherwise>
<spring:message code="policy.request.not-found" />
</c:otherwise></c:choose>