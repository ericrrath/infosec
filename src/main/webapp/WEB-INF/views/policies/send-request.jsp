<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h1>Policy: <c:out value="${policy.name}" /></h1>
<form method="post" class="form-horizontal" >
	<label>Email addresses
	<textarea name="addresses" rows="5" cols="80"></textarea>
	</label>
	<div class="form-actions">
		<button type="submit" class="btn btn-primary"><spring:message code="generic.command.add" /></button>
	</div>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>