<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<h1>
	<a href="<c:url value="/policies" />">Policies</a>:
	<c:out value="${policy.name}" />
</h1>
<form:form modelAttribute="policy" method="post">
	<p class="form-control-static">Are you sure you want to delete the policy "<c:out value="${policy.name}" />"?</p>
	<c:if test="${not empty policy.requests}">
		<p class="form-control-static">There are ${fn:length(policy.requests)} requests on this policy.</p>
	</c:if>
	<div class="form-group">
		<button type="submit" class="btn btn-primary"><spring:message code="generic.command.delete" /></button>
		<button type="submit" name="_cancel" value="_cancel" class="btn"><spring:message code="generic.command.cancel" /></button>
	</div>
</form:form>
