<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="tilesx" uri="http://tiles.apache.org/tags-tiles-extras"%>
<tilesx:useAttribute name="navItem" />
<tilesx:useAttribute name="titleCode" />
<spring:eval var="appVersion" expression="@environment.getProperty('info.build.version')" />
<spring:eval var="gitCommitId" expression="@environment.getProperty('git.commit.id.abbrev')" />
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<c:url value="/assets/img/favicon.ico" />" />
	<link href="<c:url value="/assets/styles/all.css?v=${appVersion}" />" rel="stylesheet">
	<c:if test="${not empty titleCode}">
	<title><spring:message code="${titleCode}" /></title>
	</c:if>
</head>
<body>
	<div class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href='<c:url value="/" />'><spring:message code="application.title" text="infosec" /></a>
			</div>
			<div class="navbar-collapse collapse">
				<sec:authorize access="isAuthenticated()">
				<ul class="nav navbar-nav">
					<li <c:if test="${navItem eq 'persons'}">class="active"</c:if>><a href="<c:url value="/persons" />"><spring:message code="persons" /></a></li>
					<li <c:if test="${navItem eq 'policies'}">class="active"</c:if>><a href="<c:url value="/policies" />"><spring:message code="policies" /></a></li>
					<li <c:if test="${navItem eq 'devices'}">class="active"</c:if>><a href="<c:url value="/devices" />"><spring:message code="devices" /></a></li>
					<li <c:if test="${navItem eq 'search'}">class="active"</c:if>><a href="<c:url value="/search" />"><spring:message code="search" /></a></li>
				</ul>
				<form action="<c:url value="/signout" />" method="post" class="navbar-form navbar-right">
					<label>
						<sec:authorize access="hasRole('ROLE_LOCAL')">
						<a href="<c:url value="/changePassword" />"><sec:authentication property="name" /></a>
						</sec:authorize>
						<sec:authorize access="NOT hasRole('ROLE_LOCAL')">
						<sec:authentication property="name" />
						</sec:authorize>
					</label>
					<button type="submit" class="btn">Sign out</button>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				</form>
				</sec:authorize>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<div class="container">
		<tiles:insertAttribute name="body" />
	</div>
	<footer class="footer">
		<div class="container">
			<p class="text-muted">v<c:out value="${appVersion}-${gitCommitId}" /></p>
		</div>
	</footer>
	<script src="<c:url value="/assets/scripts/all-min.js?v=${gitCommitId}" />"></script>
	<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('input[data-type=date]').datepicker({format: 'yyyy-mm-dd', autoclose: true});
	});
	</script>
</body>
</html>
